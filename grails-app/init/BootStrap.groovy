import com.pahm.SecRole
import com.pahm.SecUser
import com.pahm.SecUserSecRole

class BootStrap {

    def init = { servletContext ->
        ['ROLE_Maestro', 'ROLE_Administrador'].each { roleStr ->
            if ( !SecRole.findByAuthority(roleStr) ) {
                new SecRole(authority: roleStr).save(flush: true)
            }
        }
        
        SecUser admin1 =new SecUser(username: "jinjoglez@gmail.com", password: "l0ck3rzj1nj0", accountExpired: false, accountLocked: false, passwordExpired: false).save(flush: true)
        SecRole roleAdmin1 = SecRole.findByAuthority('ROLE_Administrador');
        SecUserSecRole.create(admin1,roleAdmin1,true)

    }
    def destroy = {
    }
}
