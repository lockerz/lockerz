package com.pahm

class Licenciatura {
    String nombre
    boolean activo
    static hasMany = [materias:Materia, grupos:Grupo]
    static constraints = {
        activo defaultValue: true
    }

    def beforeInsert(){
        this.nombre=this.nombre.toUpperCase()
    }

    def beforeUpdate(){
        this.nombre=this.nombre.toUpperCase()
    }

    void setNombre(String nombre){
        this.nombre=nombre.toUpperCase()
    }
}
