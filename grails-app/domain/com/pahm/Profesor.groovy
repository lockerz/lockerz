package com.pahm

import grails.databinding.BindingFormat

class Profesor {
    String cedula
    String nombres
    String apellidos
    String correoElectronico
    String comentario
    boolean activo
    @BindingFormat("yyyy-MM-dd")
    Date fechaCierreProfesor
    SecUser usuario
    String telefono
    String avatar="avatars/user.png"
    String passwordInicial
    @BindingFormat("yyyy-MM-dd")
    Date fechaGuardadoDisponibilidad

    static hasMany = [disponibilidad:DisponibilidadProfesor]

    static constraints = {
        telefono (nullable: true, size: 10..10)
        correoElectronico (email: true, unique: true)
        comentario (nullable: true)
        fechaCierreProfesor (nullable: true)
        correoElectronico (unique: true)
        fechaGuardadoDisponibilidad(nullable: true)
        usuario nullable: true
        passwordInicial nullable: true
    }
}
