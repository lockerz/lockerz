package com.pahm

class MateriaProfesorGrupo {
    Profesor profesor
    Materia materia
    Grupo grupo
    Boolean documentosDisponibles=false
    String planeacionMagisterial
    String programaSintetico

    static constraints = {
        planeacionMagisterial nullable: true
        programaSintetico nullable: true
    }


}
