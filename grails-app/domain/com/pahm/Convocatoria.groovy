package com.pahm

import grails.databinding.BindingFormat


class Convocatoria {
    @BindingFormat("yyyy-MM-dd")
    Date fechaInicio
    @BindingFormat("yyyy-MM-dd")
    Date fechaCierre
    String periodo
    int anio
    boolean actual
    String calendario
    String herramientaPlaneacion
    String programaSintetico

    static constraints = {
        anio(min: 2015)
        actual defaultValue: true
        periodo inList: ["Enero-Junio", "Agosto-Diciembre"]
        calendario nullable: true
        herramientaPlaneacion nullable: true
        programaSintetico nullable: true
    }
    /*Secuencia para cambiar las fechas en las que los profesores pueden
        subir su disponibilidad basado en los cambios hechos en la convocatoria
     */
    def beforeUpdate(){
        if(this.isDirty('fechaCierre')&& this.actual){
            Profesor.executeUpdate("update Profesor set fecha_cierre_profesor= ?",[this.fechaCierre])
        }
    }
}
