package com.pahm

class Configuracion {
    String nombre
    int semestres
    int horas
    static constraints = {
        nombre unique: true
    }

}
