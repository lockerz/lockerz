package com.pahm

class Materia {
    String codigo
    String nombre
    boolean activo
    String temario
    String bibliografia
    Integer numeroHoras
    Integer semestre
    static belongsTo = [licenciatura:Licenciatura]

    static constraints = {
        semestre inList: [1,2,3,4,5,6,7,8,9,10,11]
        numeroHoras inList: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
        activo defaultValue: true
        codigo unique: true
        temario nullable: true
        bibliografia nullable: true
    }

    def beforeInsert(){
        this.codigo=this.codigo.toUpperCase()
        this.nombre=this.nombre.toUpperCase()
    }

    def beforeUpdate(){
        this.codigo=this.codigo.toUpperCase()
        this.nombre=this.nombre.toUpperCase()
    }

    void setCodigo(String codigo){
        this.codigo=codigo.toUpperCase()
    }

    void setNombre(String nombre){
        this.nombre=nombre.toUpperCase()
    }
}
