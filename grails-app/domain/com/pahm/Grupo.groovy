package com.pahm

class Grupo {
    String nombre
    int semestre
    boolean activo
    String turno
    Aula aula
    boolean horarioTerminado=false
    static belongsTo = [licenciatura:Licenciatura]
    static hasMany = [horario:HorarioGrupo]

    static constraints = {
        aula nullable: true
        semestre inList: [1,2,3,4,5,6,7,8,9]
        turno inList: ['Matutino','Vespertino']
        nombre unique: true
        activo defaultValue: true
    }

    def beforeInsert(){
        this.nombre=this.nombre.toUpperCase()
    }

    def beforeUpdate(){
        this.nombre=this.nombre.toUpperCase()
    }

    void setNombre(String nombre){
        this.nombre=nombre.toUpperCase()
    }

}
