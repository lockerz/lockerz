package com.pahm

class DisponibilidadProfesor {

    String indice
    Boolean disponible
    Materia materia

    static belongsTo = [profesor:Profesor]

    static constraints = {
        materia(nullable: true)

    }
}
