package com.pahm

import grails.gorm.DetachedCriteria

class PreferenciaMaterias {

    Profesor profesor
    Materia materia
    
    PreferenciaMaterias(Profesor p, Materia m){
        this()
        profesor = p
        materia = m
    }


    static PreferenciaMaterias get(long profesorId, long materiaId) {
        criteriaFor(profesorId, materiaId).get()
    }

    static boolean exists(long profesorId, long materiaId) {
        criteriaFor(profesorId, materiaId).count()
    }

    private static DetachedCriteria criteriaFor(long profesorId, long materiaId) {
        PreferenciaMaterias.where {
            profesor == Profesor.load(profesorId) &&
                    materia == Materia.load(materiaId)
        }
    }

    static PreferenciaMaterias create(Profesor profesor, Materia materia, boolean flush = false) {
        def instance = new PreferenciaMaterias(profesor: profesor, materia: materia)
        instance.save(flush: flush, insert: true)
        instance
    }

    static boolean remove(Profesor u, Materia r, boolean flush = false) {
        if (u == null || r == null) return false

        int rowCount = PreferenciaMaterias.where { profesor == u && materia == r }.deleteAll()

        if (flush) { PreferenciaMaterias.withSession { it.flush() } }

        rowCount
    }

    static void removeAll(Profesor u, boolean flush = false) {
        if (u == null) return

        PreferenciaMaterias.where { profesor == u }.deleteAll()

        if (flush) { PreferenciaMaterias.withSession { it.flush() } }
    }

    static void removeAll(Materia r, boolean flush = false) {
        if (r == null) return

        PreferenciaMaterias.where { materia == r }.deleteAll()

        if (flush) { PreferenciaMaterias.withSession { it.flush() } }
    }

    static constraints = {

    }
}
