package com.pahm

class Aula {

    String nombre
    boolean ocupado
    boolean activo



    static constraints = {
        nombre unique: true
        activo defaultValue: true
    }

    def beforeInsert(){
        this.nombre=this.nombre.toUpperCase()
    }

    def beforeUpdate(){
        this.nombre=this.nombre.toUpperCase()
    }

    void setNombre(String nombre){
        this.nombre=nombre.toUpperCase()
    }
}
