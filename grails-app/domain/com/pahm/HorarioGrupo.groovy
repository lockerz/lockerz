package com.pahm

class HorarioGrupo {

    String indice
    Materia materia

    static belongsTo = [grupo:Grupo]

    static constraints = {
        materia nullable: true
    }
    //Metodo para quitar la asignacion de hora y materia en el horario del profesor (propiedad Materia en DisponibilidadProfesor)
    def beforeUpdate(){
        def materiaPersistida=this.getPersistentValue('materia')
        if(this.isDirty('materia')&&materiaPersistida!=null){
            def profesor=MateriaProfesorGrupo.findByMateriaAndGrupo(materiaPersistida,grupo).profesor
            def hora=DisponibilidadProfesor.findByProfesorAndIndice(profesor,indice)
            hora.materia=null
            hora.save()
        }
    }

    //Metodo para asignar la asignacion de hora y materia en el horario del profesor (propiedad Materia en DisponibilidadProfesor)
    def afterUpdate(){
        if(materia!=null){
        def profesor=MateriaProfesorGrupo.findByMateriaAndGrupo(materia,grupo).profesor
        def hora=DisponibilidadProfesor.findByProfesorAndIndice(profesor,indice)
        hora.materia=materia
        hora.save()
        }
    }
}
