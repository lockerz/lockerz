<%--
  Created by IntelliJ IDEA.
  User: JUAN
  Date: 04/05/2016
  Time: 22:21
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

    <asset:stylesheet href="bootstrap.min.css" />
    <asset:stylesheet src="/css/font-awesome.min.css" />
    <asset:stylesheet src="animate.min.css" />
    <asset:stylesheet src="custom.css" />
    <asset:stylesheet src="estilo.css" />
    <asset:stylesheet src="maps/jquery-jvectormap-2.0.3.css" />
    <asset:stylesheet src="icheck/flat/green.css" />
    <asset:stylesheet src="floatexamples.css" />
    <asset:javascript src="jquery.min.js"/>



    <g:layoutHead/>
<body>
<g:layoutBody/>


<g:pageProperty name="page.javascript"/>
</body>
</html>