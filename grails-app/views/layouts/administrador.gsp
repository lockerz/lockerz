<%--
  Created by IntelliJ IDEA.
  User: JUAN
  Date: 09/04/2016
  Time: 13:12
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        Administrador &raquo;
    </title>

    <!-- eeeeeeeeeeeeeeeeeeeeee --->

    <asset:stylesheet href="bootstrap.min.css" />
    <asset:stylesheet src="/css/font-awesome.min.css" />


    <asset:stylesheet src="animate.min.css" />
    <asset:stylesheet src="custom.css" />
    <asset:stylesheet src="estilo.css" />
    <asset:stylesheet src="maps/jquery-jvectormap-2.0.3.css" />
    <asset:stylesheet src="icheck/flat/green.css" />
    <asset:stylesheet src="floatexamples.css" />
    <asset:javascript src="jquery.min.js"/>
    <asset:javascript src="ohsnap.min.js"/>



    <g:layoutHead/>
</head>

<body class="nav-md">
<div id="ohsnap"></div>
<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <g:link controller="administrador" action="index">
                        <asset:image src="icon.png" style="max-height:100%;" />
                        <asset:image src="letras.png" style="max-height:100%;" id="letras"/>

                    </g:link>
                </div>
                <div class="clearfix"></div>


                <!-- menu prile quick info -->
                <div class="profile">

                    <div class="profile_info">
                        <span>Bienvenido,</span>
                        <h2>Administrador</h2>
                    </div>
                </div>
                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3>Administración</h3>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-user"></i> Profesores <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="display: none">
                                    <li>
                                        <g:link controller="profesor" action="create">Alta de Profesor</g:link>
                                    </li>
                                    <li><g:link controller="profesor" action="index">Lista de Profesores</g:link>
                                    </li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-university"></i>Aula <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="display: none">
                                    <li><g:link controller="aula" action="create">Alta de Aula</g:link>
                                    </li>
                                    <li><g:link controller="aula" action="index">Lista de Aulas</g:link>
                                    </li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-users"></i> Grupo <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="display: none">
                                    <li><g:link controller="grupo" action="create">Alta de Grupo</g:link>
                                    </li>
                                    <li><g:link controller="grupo" action="index">Lista de Grupos</g:link>
                                    </li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-folder-open"></i> Licenciatura <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="display: none">
                                    <li><g:link controller="licenciatura" action="create">Alta de Licenciatura</g:link>
                                    </li>
                                    <li><g:link controller="licenciatura" action="index">Lista de Licenciaturas</a></g:link>
                                    </li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-book"></i> Materia <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="display: none">
                                    <li><g:link controller="materia" action="create">Alta de Materia</g:link>
                                    </li>
                                    <li><g:link controller="materia" action="index">Lista de Materias</g:link>
                                    </li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-bullhorn"></i> Convocatoria <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="display: none">
                                    <li><g:link controller="convocatoria" action="create">Creación de convocatoria</g:link>
                                    </li>
                                    <li><g:link controller="convocatoria" action="index">Registro de convocatorias</g:link>
                                    </li>
                                    <li><g:link controller="convocatoria" action="archivos">Archivos de convocatorias</g:link>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="menu_section">
                        <h3>Horarios</h3>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-tags"></i> Asignación <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="display: none">
                                    <li><g:link controller="materiaProfesorGrupo" action="create" >Asignar Materias</g:link>
                                    </li>
                                    <li><g:link controller="materiaProfesorGrupo" action="index" >Materias Asignadas</g:link>
                                    </li>

                                </ul>
                            </li>
                            <li><g:link controller="horarioGrupo"><i class="fa fa-calendar"></i> Generar Horario</g:link></li>

                            <li><a><i class="fa fa-calendar"></i> Publicar Horario </a></li>
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                Administrador
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">

                                <li>
                                    <g:link controller="logoff"><i class="fa fa-sign-out pull-right"></i> Log Out</g:link>
                                </li>
                            </ul>
                        </li>



                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

            <br />
            <g:layoutBody/>

            <!-- footer content -->
            <footer>
                <div class="copyright-info">
                    <p class="pull-right">Lookerz - Plataforma de Administración de horarios
                    </p>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->

        </div>
        <!-- /page content -->
    </div>


</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<asset:javascript src="bootstrap.min.js"/>
<asset:javascript src="nicescroll/jquery.nicescroll.min.js"/>


<!-- bootstrap progress js -->
<asset:javascript src="progressbar/bootstrap-progressbar.min.js"/>
<!-- icheck -->
<asset:javascript src="icheck/icheck.min.js"/>
<!-- daterangepicker -->
<asset:javascript src="moment/moment.min.js"/>
<asset:javascript src="datepicker/daterangepicker.js"/>
<!-- chart js -->
<asset:javascript src="chartjs/chart.min.js"/>
<!-- sparkline -->
<asset:javascript src="sparkline/jquery.sparkline.min.js"/>

<asset:javascript src="custom.js"/>


<!-- flot js -->
<!--[if lte IE 8]><asset:javascript src="excanvas.min.js"/><![endif]-->
<asset:javascript src="flot/jquery.flot.js"/>
<asset:javascript src="flot/jquery.flot.pie.js"/>
<asset:javascript src="flot/jquery.flot.orderBars.js"/>
<asset:javascript src="flot/jquery.flot.time.min.js"/>
<asset:javascript src="flot/date.js"/>
<asset:javascript src="flot/jquery.flot.spline.js"/>
<asset:javascript src="flot/jquery.flot.stack.js"/>
<asset:javascript src="flot/curvedLines.js"/>
<asset:javascript src="flot/jquery.flot.resize.js"/>
<!-- pace -->
<asset:javascript src="pace/pace.min.js"/>
<g:pageProperty name="page.javascript"/>
<!-- flot -->
<script type="text/javascript">
    //define chart clolors ( you maybe add more colors if you want or flot will add it automatic )
    var chartColours = ['#96CA59', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'];

    //generate random number for charts
    randNum = function() {
        return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
    }

    $(function() {
        var d1 = [];
        //var d2 = [];

        //here we generate data for chart
        for (var i = 0; i < 30; i++) {
            d1.push([new Date(Date.today().add(i).days()).getTime(), randNum() + i + i + 10]);
            //    d2.push([new Date(Date.today().add(i).days()).getTime(), randNum()]);
        }

        var chartMinDate = d1[0][0]; //first day
        var chartMaxDate = d1[20][0]; //last day

        var tickSize = [1, "day"];
        var tformat = "%d/%m/%y";

        //graph options
        var options = {
            grid: {
                show: true,
                aboveData: true,
                color: "#3f3f3f",
                labelMargin: 10,
                axisMargin: 0,
                borderWidth: 0,
                borderColor: null,
                minBorderMargin: 5,
                clickable: true,
                hoverable: true,
                autoHighlight: true,
                mouseActiveRadius: 100
            },
            series: {
                lines: {
                    show: true,
                    fill: true,
                    lineWidth: 2,
                    steps: false
                },
                points: {
                    show: true,
                    radius: 4.5,
                    symbol: "circle",
                    lineWidth: 3.0
                }
            },
            legend: {
                position: "ne",
                margin: [0, -25],
                noColumns: 0,
                labelBoxBorderColor: null,
                labelFormatter: function(label, series) {
                    // just add some space to labes
                    return label + '&nbsp;&nbsp;';
                },
                width: 40,
                height: 1
            },
            colors: chartColours,
            shadowSize: 0,
            tooltip: true, //activate tooltip
            tooltipOpts: {
                content: "%s: %y.0",
                xDateFormat: "%d/%m",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            yaxis: {
                min: 0
            },
            xaxis: {
                mode: "time",
                minTickSize: tickSize,
                timeformat: tformat,
                min: chartMinDate,
                max: chartMaxDate
            }
        };
        var plot = $.plot($("#placeholder33x"), [{
            label: "Email Sent",
            data: d1,
            lines: {
                fillColor: "rgba(150, 202, 89, 0.12)"
            }, //#96CA59 rgba(150, 202, 89, 0.42)
            points: {
                fillColor: "#fff"
            }
        }], options);
    });
</script>
<!-- /flot -->
<!--  -->
<script>
    $('document').ready(function() {
        $(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 4, 5, 6, 3, 5, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
            type: 'bar',
            height: '125',
            barWidth: 13,
            colorMap: {
                '7': '#a1a1a1'
            },
            barSpacing: 2,
            barColor: '#26B99A'
        });

        $(".sparkline11").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6, 2, 4, 3, 4, 5, 4, 5, 4, 3], {
            type: 'bar',
            height: '40',
            barWidth: 8,
            colorMap: {
                '7': '#a1a1a1'
            },
            barSpacing: 2,
            barColor: '#26B99A'
        });

        $(".sparkline22").sparkline([2, 4, 3, 4, 7, 5, 4, 3, 5, 6, 2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6], {
            type: 'line',
            height: '40',
            width: '200',
            lineColor: '#26B99A',
            fillColor: '#ffffff',
            lineWidth: 3,
            spotColor: '#34495E',
            minSpotColor: '#34495E'
        });

        var doughnutData = [{
            value: 30,
            color: "#455C73"
        }, {
            value: 30,
            color: "#9B59B6"
        }, {
            value: 60,
            color: "#BDC3C7"
        }, {
            value: 100,
            color: "#26B99A"
        }, {
            value: 120,
            color: "#3498DB"
        }];

        Chart.defaults.global.legend = {
            enabled: false
        };

        var canvasDoughnut = new Chart(document.getElementById("canvas1i"), {
            type: 'doughnut',
            showTooltips: false,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: {
                labels: [
                    "Symbian",
                    "Blackberry",
                    "Other",
                    "Android",
                    "IOS"
                ],
                datasets: [{
                    data: [15, 20, 30, 10, 30],
                    backgroundColor: [
                        "#BDC3C7",
                        "#9B59B6",
                        "#455C73",
                        "#26B99A",
                        "#3498DB"
                    ],
                    hoverBackgroundColor: [
                        "#CFD4D8",
                        "#B370CF",
                        "#34495E",
                        "#36CAAB",
                        "#49A9EA"
                    ]

                }]
            }
        });

        var canvasDoughnut = new Chart(document.getElementById("canvas1i2"), {
            type: 'doughnut',
            showTooltips: false,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: {
                labels: [
                    "Symbian",
                    "Blackberry",
                    "Other",
                    "Android",
                    "IOS"
                ],
                datasets: [{
                    data: [15, 20, 30, 10, 30],
                    backgroundColor: [
                        "#BDC3C7",
                        "#9B59B6",
                        "#455C73",
                        "#26B99A",
                        "#3498DB"
                    ],
                    hoverBackgroundColor: [
                        "#CFD4D8",
                        "#B370CF",
                        "#34495E",
                        "#36CAAB",
                        "#49A9EA"
                    ]

                }]
            }
        });

        var canvasDoughnut = new Chart(document.getElementById("canvas1i3"), {
            type: 'doughnut',
            showTooltips: false,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: {
                labels: [
                    "Symbian",
                    "Blackberry",
                    "Other",
                    "Android",
                    "IOS"
                ],
                datasets: [{
                    data: [15, 20, 30, 10, 30],
                    backgroundColor: [
                        "#BDC3C7",
                        "#9B59B6",
                        "#455C73",
                        "#26B99A",
                        "#3498DB"
                    ],
                    hoverBackgroundColor: [
                        "#CFD4D8",
                        "#B370CF",
                        "#34495E",
                        "#36CAAB",
                        "#49A9EA"
                    ]

                }]
            }
        });
    });
</script>
<!-- -->
<!-- datepicker -->
<script type="text/javascript">
    $(document).ready(function() {

        var cb = function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
        }

        var optionSet1 = {
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2012',
            maxDate: '12/31/2015',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
            console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
            console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
            console.log("cancel event fired");
        });
        $('#options1').click(function() {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
            $('#reportrange').data('daterangepicker').remove();
        });
    });
</script>


</body>
</html>
