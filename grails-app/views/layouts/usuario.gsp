<%--
  Created by IntelliJ IDEA.
  User: JUAN
  Date: 09/04/2016
  Time: 13:13
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        Lockerz &raquo;
    </title>

    <!-- eeeeeeeeeeeeeeeeeeeeee --->

    <asset:stylesheet href="bootstrap.min.css" />
    <asset:stylesheet src="/css/font-awesome.min.css" />


    <asset:stylesheet src="animate.min.css" />
    <asset:stylesheet src="custom.css" />
    <asset:stylesheet src="estilo.css" />
    <asset:stylesheet src="maps/jquery-jvectormap-2.0.3.css" />
    <asset:stylesheet src="icheck/flat/green.css" />
    <asset:stylesheet src="floatexamples.css" />
    <asset:javascript src="jquery.min.js"/>



    <g:layoutHead/>
</head>

<body class="nav-md">
<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <g:link controller="usuario" action="index">
                        <asset:image src="icon.png" style="max-height:100%;" />
                        <asset:image src="letras.png" style="max-height:100%;" id="letras"/>

                    </g:link>
                </div>
                <div class="clearfix"></div>


                <!-- menu prile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <g:img dir="images" file="${profesor.avatar}" class="img-circle profile_img"/>

                    </div>
                    <div class="profile_info">
                        <span>Bienvenido,</span>
                        <h2>${profesor.nombres}</h2>
                    </div>
                </div>
                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3>Administración</h3>
                        <ul class="nav side-menu">
                            <li><g:link controller="usuario" action="editDisponibilidad"><i class="fa fa-clock-o"></i> Disponibilidad </g:link>
                            </li>
                            <li><g:link controller="usuario" action="materiasAsignadas"><i class="fa fa-file-text"></i> Materias asignadas</g:link>
                            </li>

                        </ul>
                    </div>
                    <div class="menu_section">
                        <h3>Horarios</h3>
                        <ul class="nav side-menu">
                            <li><g:link controller="usuario" action="index"><i class="fa fa-calendar"></i> Horario </g:link>
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                Profesor
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                <li><g:link controller="usuario" action="miPerfil">Perfil</g:link>
                                </li>
                                <li>
                                    <g:link controller="logoff"><i class="fa fa-sign-out pull-right"></i> Salir</g:link>
                                </li>
                            </ul>
                        </li>



                            </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

            <br />
        <!-- Aquí empieza la página en el layout de Grails-->
            <g:layoutBody/>

            <!-- footer content -->
            <footer>
                <div class="copyright-info">
                    <p class="pull-right">Lookerz - Plataforma de Administración de horarios
                    </p>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->

        </div>
        <!-- /page content -->
    </div>


</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<asset:javascript src="bootstrap.min.js"/>
<asset:javascript src="nicescroll/jquery.nicescroll.min.js"/>


<!-- bootstrap progress js -->
<asset:javascript src="progressbar/bootstrap-progressbar.min.js"/>
<!-- icheck -->
<asset:javascript src="icheck/icheck.min.js"/>
<!-- daterangepicker -->
<asset:javascript src="moment/moment.min.js"/>
<asset:javascript src="datepicker/daterangepicker.js"/>
<!-- chart js -->
<asset:javascript src="chartjs/chart.min.js"/>
<!-- sparkline -->
<asset:javascript src="sparkline/jquery.sparkline.min.js"/>

<asset:javascript src="custom.js"/>


<!-- flot js -->
<!--[if lte IE 8]><asset:javascript src="excanvas.min.js"/><![endif]-->

<!-- pace -->
<asset:javascript src="pace/pace.min.js"/>
<g:pageProperty name="page.javascript"/>

<!-- datepicker -->
<script type="text/javascript">
    $(document).ready(function() {

        var cb = function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
        }

        var optionSet1 = {
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2012',
            maxDate: '12/31/2015',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
            console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
            console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
            console.log("cancel event fired");
        });
        $('#options1').click(function() {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
            $('#reportrange').data('daterangepicker').remove();
        });
    });
</script>

</body>
</html>