<!DOCTYPE html>
<html>
    <head>
        
        <title>Lista aulas</title>
    </head>
    <body>
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Aulas <small></small></h3>
            </div>


        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Lista de Aulas</h2>

                        <div class="clearfix"></div>
                    </div>
                    <g:if test="${flash.message}">
                        <div class="alert alert-dismissible alert-warning">
                            <p>${flash.message}</p>
                        </div>
                    </g:if>
                    <div class="x_content">

                        <!-- start project list -->
                        <table class="table table-striped table-hover projects">
                            <thead>
                            <tr>
                                <th style="width:40%;">Aula</th>
                                <th>Estatus</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${aulas?}" var="aula">
                                <tr>
                                    <td>
                                        <h4>
                                            <g:link action="show" id="${aula.id}">
                                                ${aula.nombre}
                                            </g:link>
                                        </h4>
                                    </td>

                                    <td>
                                        <g:if test="${aula.activo}">
                                            <h4><span class="label label-success">Activo</span></h4>
                                        </g:if>
                                        <g:else>
                                            <h4><span class="label label-danger">Inactivo</span></h4>
                                        </g:else>

                                    </td>
                                    <td>
                                        <g:form resource="${aula}" method="DELETE">
                                            <g:link action="edit" id="${aula.id}" class="btn btn-success"> Editar</g:link>
                                            <input class="btn btn-danger" type="submit" value="Eliminar" onclick="return confirm('Esta seguro de eliminar este registro?');" />
                                        </g:form>
                                    </td>
                                </tr>
                            </g:each>

                            </tbody>
                        </table>
                        <!-- end project list -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>