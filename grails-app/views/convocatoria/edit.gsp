<!DOCTYPE html>
<html>
    <head>

        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Convocatorias
                </h3>
            </div>

        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Modificar Convocatoria </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <g:hasErrors bean="${this.convocatoria}">
                            <ul class="errors" role="alert">
                                <g:eachError bean="${this.convocatoria}" var="error">
                                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                                </g:eachError>
                            </ul>
                        </g:hasErrors>

                        <g:form action="update" method="PUT" class="form-horizontal form-label-left" novalidate="true">
                            <input type="hidden" value="${convocatoria.id}" name="convocatoria.id">
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Periodo <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <g:select name="convocatoria.periodo" from="${["Enero-Junio", "Agosto-Diciembre"]}" required="true" value="${convocatoria.periodo}" class="form-control col-md-7 col-xs-12" />
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Año <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <g:select name="convocatoria.anio" from="${2016..2099}" value="${convocatoria.anio}" class="form-control col-md-7 col-xs-12" />
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Fecha de Apertura <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input  class="form-control col-md-2 col-xs-12" name="convocatoria.fechaInicio" value="${convocatoria.fechaInicio.format('yyyy-MM-dd')}" required="required" type="date">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Fecha de Cierre <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input class="form-control col-md-2 col-xs-12"  name="convocatoria.fechaCierre" value="${convocatoria.fechaCierre.format('yyyy-MM-dd')}" required="required" type="date">
                                </div>
                            </div>


                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                    <!--<button type="submit" class="btn btn-primary">Cancelar</button>-->
                                    <button id="send" type="submit" class="btn btn-success">Aceptar</button>
                                </div>
                            </div>
                        </g:form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <content tag="javascript">
        <asset:javascript src="validator/validator.js"/>

        <script>
            // initialize the validator function
            validator.message['date'] = 'not a real date';

            // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
            $('form')
                    .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                    .on('change', 'select.required', validator.checkField)
                    .on('keypress', 'input[required][pattern]', validator.keypress);

            $('.multi.required')
                    .on('keyup blur', 'input', function() {
                        validator.checkField.apply($(this).siblings().last()[0]);
                    });

            // bind the validation to the form submit event
            //$('#send').click('submit');//.prop('disabled', true);

            $('form').submit(function(e) {
                e.preventDefault();
                var submit = true;
                // evaluate the form using generic validaing
                if (!validator.checkAll($(this))) {
                    submit = false;
                }

                if (submit)
                    this.submit();
                return false;
            });

            /* FOR DEMO ONLY */
            $('#vfields').change(function() {
                $('form').toggleClass('mode2');
            }).prop('checked', false);

            $('#alerts').change(function() {
                validator.defaults.alerts = (this.checked) ? false : true;
                if (this.checked)
                    $('form .alert').remove();
            }).prop('checked', false);
        </script>

    </content>
    </body>
</html>
