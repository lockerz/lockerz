<!DOCTYPE html>
<html>
    <head>
       
        <title>Lista de convocatorias</title>
    </head>
    <body>
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Convocatorias <small></small></h3>
            </div>


        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Lista de Convocatorias</h2>

                        <div class="clearfix"></div>
                    </div>
                    <g:if test="${flash.message}">
                        <div class="alert alert-dismissible alert-success">
                            <p>${flash.message}</p>
                        </div>
                    </g:if>
                    <div class="x_content">

                        <!-- start project list -->
                        <table class="table table-striped table-hover projects">
                            <thead>
                            <tr>
                                <th style="width:50%;">Periodo</th>
                                <th>Año</th>
                                <th>Actual</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${convocatorias?}" var="convocatoria">
                                <tr>
                                    <td>
                                        <h4>
                                            <g:link action="show" id="${convocatoria.id}">
                                                ${convocatoria.periodo}
                                            </g:link>
                                        </h4>
                                    </td>
                                    <td>
                                        <h4>

                                                ${convocatoria.anio}

                                        </h4>
                                    </td>
                                    <td>
                                        <g:if test="${convocatoria.actual}">
                                            <h4><span class="label label-success">Actual</span></h4>
                                        </g:if>
                                        <g:else>
                                            <h4><span class="label label-danger">Finalizada</span></h4>
                                        </g:else>

                                    </td>
                                    <td>
                                        <g:form resource="${convocatoria}" method="DELETE">
                                            <g:link action="edit" id="${convocatoria.id}" class="btn btn-success"> Editar</g:link>
                                            <input class="btn btn-danger" type="submit" value="Eliminar" onclick="return confirm('Esta seguro de eliminar este registro?');" />
                                        </g:form>
                                    </td>
                                </tr>
                            </g:each>

                            </tbody>
                        </table>
                        <!-- end project list -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>