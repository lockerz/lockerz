<%--
  Created by IntelliJ IDEA.
  User: JUAN
  Date: 05/06/2016
  Time: 19:10
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Subir documentos <small></small></h3>
        </div>


    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Subir Calendario, Herramienta de Planeación Magisterial y Programa Sintético</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <g:if test="${flash.message}">
                    <div class="alert alert-dismissible alert-success">
                        <p>${flash.message}</p>
                    </div>
                </g:if>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">A continuación podrás subir el Calendario, Herramienta de Planeación Magisterial y Programa Sintético del periodo en curso. </p>

                    <!-- start project list -->
                    <div class="col-md-12">
                        <g:form action="uploadArchivos" method="post" enctype="multipart/form-data" >
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th><h4>Periodo: </h4></th>
                                    <td>${convocatoria?.periodo} ${convocatoria?.anio}</td>
                                </tr>

                                <tr>
                                    <th><h4>Calendario (.pdf): </h4></th>
                                    <td>
                                        <div class="form-group">
                                            <label>Elegir Archivo</label>
                                            <input type="file" name="calendario">

                                        </div>
                                    </td>
                                    <td>
                                        <g:if test="${convocatoria?.calendario!=null}">
                                            <a href="/static/documents/${convocatoria.calendario}" class="btn btn-primary" target="_blank"><i class="fa fa-calendar" aria-hidden="true"></i> Ver Calendario Universidad</a>
                                        </g:if>
                                    </td>
                                </tr>
                                <tr>
                                    <th><h4>Herramienta de Planeacion Magisterial (.docx)</h4></th>
                                    <td>
                                        <div class="form-group">
                                            <label >Elegir Archivo</label>
                                            <input type="file" name="herramientaPlaneacion">

                                        </div>
                                    </td>
                                    <td>
                                        <g:if test="${convocatoria?.herramientaPlaneacion!=null}">
                                            <a href="/static/documents/${convocatoria.herramientaPlaneacion}" class="btn btn-primary" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i> Ver Herramienta</a>
                                        </g:if>
                                    </td>
                                </tr>
                                <tr>
                                    <th><h4>Programa Sintetico (.docx)</h4></th>
                                    <td>
                                        <div class="form-group">
                                            <label >Elegir Archivo</label>
                                            <input type="file" name="programaSintetico">

                                        </div>
                                    </td>
                                    <td>
                                        <g:if test="${convocatoria?.programaSintetico!=null}">
                                            <a href="/static/documents/${convocatoria.programaSintetico}" class="btn btn-primary" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i> Ver Programa Sintético</a>
                                        </g:if>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-success">Subir</button>
                            </div>
                        </g:form>
                    </div>
                    <!-- end project list -->
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>