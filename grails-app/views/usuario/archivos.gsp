<%--
  Created by IntelliJ IDEA.
  User: JUAN
  Date: 06/06/2016
  Time: 15:47
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Subir documentos <small></small></h3>
        </div>


    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Subir planeación magisterial y programa sintético</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <g:if test="${flash.message}">
                    <div class="alert alert-dismissible alert-success">
                        <p>${flash.message}</p>
                    </div>
                </g:if>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">A continuación podrás subir la planeación magisterial y el programa sintético. </p>

                    <!-- start project list -->
                    <div class="col-md-12">
                        <g:form action="uploadArchivos" method="post" enctype="multipart/form-data" >
                            <input type="hidden" value="${materiaProfesorGrupo.id}" name="materiaProfesorGrupo.id">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th><h4>Sigla</h4></th>
                                    <td>${materiaProfesorGrupo.materia.codigo}</td>
                                </tr>
                                <tr>
                                    <th><h4>Materia</h4></th>
                                    <td>${materiaProfesorGrupo.materia.nombre}</td>
                                </tr>
                                <tr>
                                <tr>
                                    <th><h4>Planeación Magisterial (Documento de Word .docx): </h4></th>
                                    <td>
                                        <div class="form-group">
                                            <label>Elegir Archivo</label>
                                            <input type="file" name="planeacionMagisterial">
                                        </div>
                                    </td>
                                    <td>
                                        <g:if test="${materiaProfesorGrupo.planeacionMagisterial!=null}">
                                            <a href="/static/documents/${materiaProfesorGrupo.planeacionMagisterial}" class="btn btn-primary" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i> Ver Pnaeación Magisterial</a>
                                        </g:if>
                                    </td>
                                </tr>
                                <tr>
                                    <th><h4>Programa Sitentico (Documento de Word .docx)</h4></th>
                                    <td>
                                        <div class="form-group">
                                            <label >Elegir Archivo</label>
                                            <input type="file" name="programaSintetico">
                                        </div>
                                    </td>
                                    <td>
                                        <g:if test="${materiaProfesorGrupo.programaSintetico!=null}">
                                            <a href="/static/documents/${materiaProfesorGrupo.programaSintetico}" class="btn btn-primary" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i> Ver Programa Sintetico</a>
                                        </g:if>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-success">Subir</button>
                            </div>
                        </g:form>
                    </div>
                    <!-- end project list -->
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>