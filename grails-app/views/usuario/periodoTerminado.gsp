<%--
  Created by IntelliJ IDEA.
  User: JUAN
  Date: 06/06/2016
  Time: 0:47
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Disponibilidad de profesores </h3>
        </div>

    </div>
    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Alta de Disponibilidad</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <div class="bs-example" data-example-id="simple-jumbotron">
                        <div class="jumbotron">
                            <h1>Periodo ${convocatoria.periodo} ${convocatoria.anio} cerrado. </h1>
                            <p>Lo sentimos, el periodo de entrega de disponibilidad <strong>ha terminado.</strong>
                                Si deaseas subir tu disponibilidad o hacer cambios debes acercarte
                                directamente a la <strong>coordinación de ingenierías</strong> a solicitar un plazo
                            especial para realizar cualquiera de estas acciones.
                            </p>
                            <p>
                                Por su comprensión, gracias.
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

</body>
</html>