<%--
  Created by IntelliJ IDEA.
  User: JUAN
  Date: 21/05/2016
  Time: 20:59
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Materias asignadas a profesor <small></small></h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <!-- empieza recuadro blanco -->
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Recursos para elaboración de planificación magisterial</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <!-- start project list -->
                    <div class="col-md-12">
                        <table class="table projects disponibilidad">
                            <thead>
                            <tr>
                                <th class="col-md-2 text-center">Calendario Universidad Periodo "${convocatoria?.periodo} ${convocatoria?.anio}"</th>
                                <th class="col-md-2 text-center">Herramienta de planeación magisterial</th>
                                <th class="col-md-2 text-center">Programa Sintético</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-center">
                                    <g:if test="${convocatoria?.calendario!=null}">
                                        <a href="/static/documents/${convocatoria.calendario}" class="btn btn-primary" target="_blank"><i class="fa fa-calendar" aria-hidden="true"></i> Ver Calendario Universidad</a>
                                    </g:if>
                                </td>
                                <td class="text-center">
                                    <g:if test="${convocatoria?.herramientaPlaneacion!=null}">
                                        <a href="/static/documents/${convocatoria.herramientaPlaneacion}" class="btn btn-primary" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i> Ver Herramienta</a>
                                    </g:if>
                                </td>
                                <td class="text-center">
                                    <g:if test="${convocatoria?.programaSintetico!=null}">
                                        <a href="/static/documents/${convocatoria.programaSintetico}" class="btn btn-primary" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i> Ver Programa Sintético</a>
                                    </g:if>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- end project list -->
                </div>
            </div>
        </div>
    </div>

    <!-- empieza recuadro blanco -->
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Materias Asignadas</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">A continuación se muestran una lista de las clases que han sido asignadas para este semestre. </p>

                    <!-- start project list -->
                    <div class="col-md-12">
                        <table class="table projects disponibilidad">
                            <thead>
                            <tr>
                                <th class="col-md-2">Sigla</th>
                                <th class="col-md-2">Materia</th>
                                <th class="col-md-2">Horas por semana</th>
                                <th class="col-md-2">Temario</th>
                                <th class="col-md-2">Bibliografía</th>
                                <th class="col-md-2">Planeación Magisterial</th>
                            </tr>
                            </thead>
                            <tbody>

                            <g:each in="${materiasAsignadas}" var="asignacion">
                            <tr>
                                <td>
                                    <strong>${asignacion?.materia.codigo}</strong>
                                </td>
                                <td>
                                    ${asignacion?.materia.nombre}
                                </td>
                                <td class="text-center">
                                    ${asignacion?.materia.numeroHoras}
                                </td>
                                <td>
                                    <g:if test="${asignacion?.materia?.temario!=null}">
                                        <a href="/static/documents/${asignacion?.materia.temario}" class="btn btn-primary" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i> Ver Temario</a>
                                    </g:if>
                                </td>
                                <td>
                                    <g:if test="${asignacion?.materia.bibliografia!=null}">
                                        <a href="/static/documents/${asignacion?.materia.bibliografia}" class="btn btn-primary" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i> Ver Bibliografia</a>
                                    </g:if>
                                </td>
                                <td>
                                    <g:link action="archivos" id="${asignacion.id}" class="btn btn-warning"><i class="fa fa-upload" aria-hidden="true"></i> Subir planeación</g:link>
                                </td>
                            </tr>
                            </g:each>
                            </tbody>
                        </table>
                    </div>
                    <!-- end project list -->
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>