<%--
  Created by IntelliJ IDEA.
  User: JUAN
  Date: 21/05/2016
  Time: 19:31
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>

</head>
<body>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Disponibilidad de profesores <small></small></h3>
        </div>


    </div>
    <div class="clearfix"></div>
    <g:form action="updateDisponibilidad" method="POST">
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Alta de Disponibilidad</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <g:if test="${flash.message}">
                    <div class="alert alert-dismissible alert-success">
                        <strong>${flash.message}</strong>
                    </div>
                </g:if>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">Da click en los módulos disponibles para impartir clase. </p>

                    <!-- start project list -->
                    <div class="col-md-10 col-md-offset-1">
                        <table class="table table-bordered projects text-center disponibilidad">
                            <thead>
                            <tr>
                                <th class="col-md-2 text-center">Módulo</th>
                                <th class="col-md-2 text-center">Lunes</th>
                                <th class="col-md-2 text-center">Martes</th>
                                <th class="col-md-2 text-center">Miércoles</th>
                                <th class="col-md-2 text-center">Jueves</th>
                                <th class="col-md-2 text-center">Viernes</th>
                            </tr>
                            </thead>
                            <tbody>
                            <%id=0%>
                            <g:each in="${(7..20)}" var="hora">
                                <tr >
                                    <td>${hora<10?'0':''}${hora}:00-${(hora+1)<10?'0':''}${hora+1}:00</td>
                                    <td id="tdL${hora}" onclick="cambiarDisponibilidad('L${hora}')" class="${disponibilidad.find{it.indice==("L"+hora)}.disponible?'disponible':''}"></td>
                                        <input type="hidden" name="profesor.disponibilidad[${id}].id" value="${disponibilidad.find{it.indice==("L"+hora)}.id}">
                                        <input type="hidden" id="inputL${hora}" name="profesor.disponibilidad[${id}].disponible" value="${disponibilidad.find{it.indice==("L"+hora)}.disponible}">
                                        <%id++%>
                                    <td id="tdM${hora}" onclick="cambiarDisponibilidad('M${hora}')" class="${disponibilidad.find{it.indice==("M"+hora)}.disponible?'disponible':''}"></td>
                                        <input type="hidden" name="profesor.disponibilidad[${id}].id" value="${disponibilidad.find{it.indice==("M"+hora)}.id}">
                                        <input type="hidden" id="inputM${hora}" name="profesor.disponibilidad[${id}].disponible" value="${disponibilidad.find{it.indice==("M"+hora)}.disponible}">
                                        <%id++%>
                                    <td id="tdX${hora}" onclick="cambiarDisponibilidad('X${hora}')" class="${disponibilidad.find{it.indice==("X"+hora)}.disponible?'disponible':''}"></td>
                                        <input type="hidden" name="profesor.disponibilidad[${id}].id" value="${disponibilidad.find{it.indice==("X"+hora)}.id}">
                                        <input type="hidden" id="inputX${hora}" name="profesor.disponibilidad[${id}].disponible" value="${disponibilidad.find{it.indice==("X"+hora)}.disponible}">
                                        <%id++%>
                                    <td id="tdJ${hora}" onclick="cambiarDisponibilidad('J${hora}')" class="${disponibilidad.find{it.indice==("J"+hora)}.disponible?'disponible':''}"></td>
                                        <input type="hidden" name="profesor.disponibilidad[${id}].id" value="${disponibilidad.find{it.indice==("J"+hora)}.id}">
                                        <input type="hidden" id="inputJ${hora}" name="profesor.disponibilidad[${id}].disponible" value="${disponibilidad.find{it.indice==("J"+hora)}.disponible}">
                                        <%id++%>
                                    <td id="tdV${hora}" onclick="cambiarDisponibilidad('V${hora}')" class="${disponibilidad.find{it.indice==("V"+hora)}.disponible?'disponible':''}"></td>
                                        <input type="hidden" name="profesor.disponibilidad[${id}].id" value="${disponibilidad.find{it.indice==("V"+hora)}.id}">
                                        <input type="hidden" id="inputV${hora}" name="profesor.disponibilidad[${id}].disponible" value="${disponibilidad.find{it.indice==("V"+hora)}.disponible}">
                                        <%id++%>
                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                    </div>
                    <!-- end project list -->

                    <div class="row text-right">
                        <div class="col-md-12">
                            <asset:image src="icondisp.png" style="height:30px; display:inline-block;"/>
                            <h5 style="display:inline-block;"><em>Horario disponible para impartir clase</em></h5>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Selección de Materias</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">A continuación selecciona las materias que te gustaría impartir este semestre. </p>

                    <!-- start project list -->
                    <div class="row materias">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-12 col-xs-12">Licenciatura</label>
                                <div class="col-sm-12 col-xs-12">
                                    <g:select name="materia.licenciatura" from="${licenciaturas}" optionValue="nombre" optionKey="id" class="form-control" onchange="ajaxListaMaterias(this)"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row materias">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-12 col-xs-12">Materia</label>
                                <div class="col-sm-12 col-xs-12" id="divMaterias">
                                    <!--Esto es para cuando todavía no se ha seleccionado una materia -->
                                    <div class="well well-sm">
                                        Debes seleccionar una licenciatura.
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row materias">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-12 col-xs-12">Lista de Materias</label>
                                <div class="col-sm-12 col-xs-12">
                                    <table  class="table">
                                        <tbody id="listaPreferencia">
                                            <g:each in="${preferencias}" var="row" >
                                                <tr id="materia${row.materia.id}">
                                                    <td>${row.materia.nombre}<input type="hidden" name="preferencias[]" value="${row.materia.id}"></td>
                                                    <td>${row.materia.licenciatura.nombre}</td>
                                                    <td>${row.materia.semestre} semestre</td>
                                                    <td class="text-center"><span class=" btn btn-danger badge" onclick="$('#${row.materia.id}').remove()"><i class="fa fa-times"></i></span></td>
                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row materias">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-12 col-xs-12">Comentarios</label>
                                <div class="col-sm-12 col-xs-12">
                                    <g:textArea rows="4" class="form-control" placeholder="Haz tus comentarios aquí" name="profesor.comentario" value="${profesor.comentario}"/>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row materias" style="float:right;">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-12 col-xs-12">
                                    <g:link action="index" class="btn btn-info">Regresar</g:link>
                                    <button type="submit" class="btn btn-primary">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end project list -->


                </div>
            </div>
        </div>
    </div>
    </g:form>
</div>
<content tag="javascript">
    <script>
        function cambiarDisponibilidad(id){
            var elemento='#td'+id;
            $(elemento).toggleClass("disponible");
            var elemento='#input'+id;
            var val = $(elemento).val();
            $(elemento).val(val === "true" ? "false" : "true");
        }

        function ajaxListaMaterias(x){

            var lista= document.getElementById($(x).attr("id"));

            var SelectedValue = lista.options[lista.selectedIndex].value;

            $.ajax({
                url: "../usuario/ajaxListaMaterias",
                type: "POST",
                data: {
                    'licenciatura' : SelectedValue
                },

                dataType: "html",
                success: function(resultados){
                    $("#divMaterias").html(resultados);
                }
            });

        }

        function agregarLista(id,nombre,semestre,licenciatura){
            var identificador= "materia"+id;
            if ( $( '#'+identificador).length<=0) {
                    console.log($( identificador).length);

            var nuevotr = document.createElement("tr");

                nuevotr.id=identificador;

                nuevotr.innerHTML='<td>'+nombre+'<input type="hidden" name="preferencias[]" value="'+id+'"></td><td>'+licenciatura+'</td><td>'+semestre+' semestre</td><td class="text-center"><span class=" btn btn-danger badge" onclick="$('+"'"+'#'+identificador+"'"+').remove()"><i class="fa fa-times"></i></span></td>';
            document.getElementById("listaPreferencia").appendChild(nuevotr);
            }
        }

    </script>
</content>
</body>
</html>