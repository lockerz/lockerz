<%--
  Created by IntelliJ IDEA.
  User: JUAN
  Date: 21/05/2016
  Time: 18:56
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Perfil</h3>
        </div>


    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Información de perfil <small></small></h2>

                    <div class="clearfix"></div>
                </div>
                <g:if test="${flash.message}">
                    <div class="alert alert-dismissible alert-success">
                        <p>${flash.message}</p>
                    </div>
                </g:if>
                <div class="x_content">

                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">

                        <div class="profile_img">

                            <!-- end of image cropping -->
                            <div id="crop-avatar">
                                <!-- Current avatar -->
                                <div class="avatar-view" title="Cambiar Imagen">
                                    <g:img dir="images" file="${profesor.avatar}"/>
                                </div>

                                <!-- Cropping modal -->
                                <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <g:form action="updateAvatar" method="post" enctype="multipart/form-data" >

                                                <div class="modal-header">
                                                    <button class="close" data-dismiss="modal" type="button">&times;</button>
                                                    <h4 class="modal-title" id="avatar-modal-label">Cambiar foto de perfil</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="avatar-body">

                                                        <!-- Upload image and data -->
                                                        <div class="avatar-upload">
                                                            <input class="avatar-src" name="avatar_src" type="hidden">
                                                            <input class="avatar-data" name="avatar_data" type="hidden">
                                                            <label for="avatarInput">Subir archivo</label>
                                                            <input class="avatar-input" id="avatarInput" name="avatar_file" type="file">
                                                        </div>

                                                        <!-- Crop and preview -->
                                                        <div class="row">
                                                            <div class="col-md-9">
                                                                <div class="avatar-wrapper">
                                                                    <img id="blah" src="" alt="" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="avatar-preview preview-lg"></div>
                                                                <div class="avatar-preview preview-md"></div>
                                                                <div class="avatar-preview preview-sm"></div>
                                                            </div>
                                                        </div>

                                                        <div class="row avatar-btns">
                                                            <div class="col-md-9">

                                                            </div>
                                                            <div class="col-md-3">
                                                                <button class="btn btn-primary btn-block avatar-save" type="submit">Listo</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="modal-footer">
                                                  <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                                                </div> -->
                                            </g:form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.modal -->

                                <!-- Loading state -->
                                <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                            </div>
                            <!-- end of image cropping -->

                        </div>


                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">


                        <h3>${profesor.nombres} ${profesor.apellidos}</h3>

                        <ul class="list-unstyled user_data">
                            <li><i class="fa fa-envelope user-profile-icon"></i> ${profesor.correoElectronico}
                            </li>

                            <li>
                                <i class="fa fa-phone user-profile-icon"></i> ${profesor.telefono}
                            </li>

                            <li class="m-top-xs">
                                <i class="fa fa-asterisk user-profile-icon"></i>${profesor.cedula}
                            </li>
                        </ul>

                        <g:link controller="usuario" action="editPerfil" class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Editar Perfil</g:link>
                        <br />

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<content tag="javascript">
    <asset:javascript src="cropping/cropper.min.js"/>
    <asset:javascript src="cropping/main.js"/>
    <script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#avatarInput").change(function(){
        readURL(this);
    });
</script>
</content>
</body>
</html>