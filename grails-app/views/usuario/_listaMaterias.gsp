<%@ page contentType="text/html;charset=UTF-8" %>
<table class="table">
<tbody>
    <g:each in="${materias}" var="materia">
        <tr>
            <td>${materia.nombre}</td>
            <td>${materia.semestre} semestre</td>
            <td class="text-center"><span type="button" class="btn btn-primary badge" onclick="agregarLista(${materia.id},'${materia.nombre}','${materia.semestre}','${materia.licenciatura.nombre}')">Agregar a lista</span></td>
        </tr>

    </g:each>
</tbody>
</table>