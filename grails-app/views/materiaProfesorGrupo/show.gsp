<!DOCTYPE html>
<html>
    <head>

        <title>Detalle de Asignación</title>
    </head>
    <body>
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Asignación de Materias
                </h3>
            </div>

        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Detalle de Asignación de Materia </h2>
                        <div class="clearfix"></div>
                    </div>
                    <g:if test="${flash.message}">
                        <div class="alert alert-dismissible alert-success">
                            <strong>Se ha creado con éxito la siguiente asignación:</strong>
                        </div>
                    </g:if>
                    <div class="x_content">
                        <div class="form-horizontal form-label-left">

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Materia <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label class="form-control col-md-7 col-xs-12">${materiaProfesorGrupo.materia.codigo} ${materiaProfesorGrupo.materia.nombre}</label>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Profesor <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label class="form-control col-md-7 col-xs-12">${materiaProfesorGrupo.profesor.nombres} ${materiaProfesorGrupo.profesor.apellidos}</label>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Grupo <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label class="form-control col-md-7 col-xs-12">${materiaProfesorGrupo.grupo.nombre}</label>
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                <!--<button type="submit" class="btn btn-primary">Cancelar</button>-->

                                    <g:form resource="${this.materiaProfesorGrupo}" method="DELETE">
                                        <g:link action="edit" id="${materiaProfesorGrupo.id}" class="btn btn-success"> Editar</g:link>
                                        <g:link action="index" class="btn btn-info">Regresar</g:link>
                                        <input class="btn btn-danger" type="submit" value="Eliminar" onclick="return confirm('Esta seguro de eliminar este registro?');" />
                                    </g:form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
