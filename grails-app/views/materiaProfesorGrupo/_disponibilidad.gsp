<%@ page contentType="text/html;charset=UTF-8" %>


    <!-- start project list -->
    <div class="col-md-10 col-md-offset-1">
        <table class="table table-bordered projects text-center disponibilidad">
            <thead>
            <tr>
                <th class="col-md-2 text-center">Módulo</th>
                <th class="col-md-2 text-center">Lunes</th>
                <th class="col-md-2 text-center">Martes</th>
                <th class="col-md-2 text-center">Miércoles</th>
                <th class="col-md-2 text-center">Jueves</th>
                <th class="col-md-2 text-center">Viernes</th>
            </tr>
            </thead>
            <tbody>
            <%id=0%>
            <g:each in="${(7..20)}" var="hora">
                <tr >
                    <td>${hora<10?'0':''}${hora}:00-${(hora+1)<10?'0':''}${hora+1}:00</td>
                    <td id="tdL${hora}" class="${disponibilidad.find{it.indice==("L"+hora)}.disponible?'disponible':''}"></td>


                    <%id++%>
                    <td id="tdM${hora}" class="${disponibilidad.find{it.indice==("M"+hora)}.disponible?'disponible':''}"></td>


                    <%id++%>
                    <td id="tdX${hora}" class="${disponibilidad.find{it.indice==("X"+hora)}.disponible?'disponible':''}"></td>


                    <%id++%>
                    <td id="tdJ${hora}" class="${disponibilidad.find{it.indice==("J"+hora)}.disponible?'disponible':''}"></td>


                    <%id++%>
                    <td id="tdV${hora}" class="${disponibilidad.find{it.indice==("V"+hora)}.disponible?'disponible':''}"></td>


                    <%id++%>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
    <!-- end project list -->

    <div class="row text-right">
        <div class="col-md-12">
            <asset:image src="icondisp.png" style="height:30px; display:inline-block;"/>
            <h5 style="display:inline-block;"><em>Horario disponible para impartir clase</em></h5>
        </div>
    </div>


    <div class="row materias">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-12 col-xs-12">Lista de Materias</label>
                <div class="col-sm-12 col-xs-12">
                    <table  class="table">
                        <tbody id="listaPreferencia">
                        <g:each in="${preferencias}" var="row" >
                            <tr id="materia${row.materia.id}">
                                <td>${row.materia.nombre}<input type="hidden" name="preferencias[]" value="${row.materia.id}"></td>
                                <td>${row.materia.licenciatura.nombre}</td>
                                <td>${row.materia.semestre} semestre</td>
                                <td class="text-center"><span class=" btn btn-danger badge" onclick="$('#${row.materia.id}').remove()"><i class="fa fa-times"></i></span></td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


