<!DOCTYPE html>
<html>
    <head>
        <title>Editar Asignación</title>
    </head>
    <body>
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Modificar Asignación
                </h3>
            </div>

        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Modificar Asignación </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <g:hasErrors bean="${this.materiaProfesorGrupo}">
                            <ul class="errors" role="alert">
                                <g:eachError bean="${this.materiaProfesorGrupo}" var="error">
                                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                                </g:eachError>
                            </ul>
                        </g:hasErrors>

                        <g:form action="update" method="PUT" class="form-horizontal form-label-left" novalidate="true">
                            <input type="hidden" value="${materiaProfesorGrupo.id}" name="materiaProfesorGrupo.id">
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Materia <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <g:select name="materiaProfesorGrupo.materia" from="${materias}" optionValue="${{it.codigo+' '+it.nombre}}" optionKey="id" value="${materiaProfesorGrupo.materia.id}"  class="form-control col-md-7 col-xs-12" />
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Profesor <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <g:select name="materiaProfesorGrupo.profesor" from="${profesores}" optionValue="${{it.apellidos+' '+it.nombres}}" optionKey="id" value="${materiaProfesorGrupo.profesor.id}" class="form-control col-md-7 col-xs-12" />
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Grupo <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <g:select name="materiaProfesorGrupo.grupo" from="${grupos}" optionValue="nombre" optionKey="id" value="${materiaProfesorGrupo.grupo.id}" class="form-control col-md-7 col-xs-12" />
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="activo">Liberar Archivos <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="hidden" name="aula._activo"/>
                                    <input type="checkbox" id="activo" ${materiaProfesorGrupo.documentosDisponibles?"checked":""} name="materiaProfesorGrupo.documentosDisponibles" data-validate-length-range="8,20" class="form-control col-md-7 col-xs-12" >
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                    <g:link action="index" class="btn btn-info">Cancelar</g:link>
                                    <button id="send" type="submit" class="btn btn-success">Aceptar</button>
                                </div>
                            </div>
                        </g:form>

                    </div>
                </div>
            </div>
        </div>

        <!--Carga de disponibilidad del maestro asignado-->
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Disponibilidad del Profesor</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" id="divDisponibilidad"></div>
                </div>
            </div>
        </div>
    </div>
    <content tag="javascript">
        <asset:javascript src="validator/validator.js"/>
        <script>
            function ajaxDisponibilidad(x){

                var lista= document.getElementById($(x).attr("id"));

                var SelectedValue = lista.options[lista.selectedIndex].value;

                $.ajax({
                    url: "../materiaProfesorGrupo/ajaxDisponibilidadProfesor",
                    type: "POST",
                    data: {
                        'profesor' : SelectedValue
                    },

                    dataType: "html",
                    success: function(resultados){
                        $("#divDisponibilidad").html(resultados);
                    }
                });

            }
        </script>
        <script>
            // initialize the validator function
            validator.message['date'] = 'not a real date';

            // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
            $('form')
                    .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                    .on('change', 'select.required', validator.checkField)
                    .on('keypress', 'input[required][pattern]', validator.keypress);

            $('.multi.required')
                    .on('keyup blur', 'input', function() {
                        validator.checkField.apply($(this).siblings().last()[0]);
                    });

            // bind the validation to the form submit event
            //$('#send').click('submit');//.prop('disabled', true);

            $('form').submit(function(e) {
                e.preventDefault();
                var submit = true;
                // evaluate the form using generic validaing
                if (!validator.checkAll($(this))) {
                    submit = false;
                }

                if (submit)
                    this.submit();
                return false;
            });

            /* FOR DEMO ONLY */
            $('#vfields').change(function() {
                $('form').toggleClass('mode2');
            }).prop('checked', false);

            $('#alerts').change(function() {
                validator.defaults.alerts = (this.checked) ? false : true;
                if (this.checked)
                    $('form .alert').remove();
            }).prop('checked', false);
        </script>

    </content>
    </body>
</html>
