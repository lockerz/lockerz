<%--
  Created by IntelliJ IDEA.
  User: JUAN
  Date: 29/05/2016
  Time: 12:55
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>

    <title>Cambiar Password</title>
</head>
<asset:stylesheet href="bootstrap.min.css" />
<asset:stylesheet src="/css/font-awesome.min.css" />
<asset:stylesheet src="animate.min.css" />
<asset:stylesheet src="custom.css" />
<asset:stylesheet src="estilo.css" />
<asset:stylesheet src="maps/jquery-jvectormap-2.0.3.css" />
<asset:stylesheet src="icheck/flat/green.css" />
<asset:stylesheet src="floatexamples.css" />
<asset:javascript src="jquery.min.js"/>


<body style="background:#F7F7F7;">

<div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper">

        <div id="login" class="animate form">
            <g:if test='${flash.message}'>
                <div class="alert alert-info">${flash.message}</div>
            </g:if>
            <section class="login_content">
                <g:form action="updatePassword" method="POST">
                    <h1>Login</h1>
                    <div>

                        <input type="email" class="form-control" placeholder="Correo" required name="username" id="username"/>
                    </div>
                    <div>
                        <input type="password" class="form-control" placeholder="Contraseña Provisional" required="" name="currentPassword" id="password"/>
                    </div>
                    <div>
                        <input type="password" class="form-control" placeholder="Nueva Contraseña" required="" name="newPassword" id="password1"/>
                    </div>
                    <div>
                        <input type="password" class="form-control" placeholder="Confirmar Contraseña" onkeyup="checkPassword()" required="" name="confirmPassword" id="password2"/><span id="mensaje"></span>
                    </div>

                    <div>
                        <input type="submit" value="Entrar" class="btn btn-default submit" id="submit" disabled="true">
                    </div>
                    <div class="clearfix"></div>
                    <div class="separator">


                        <div class="clearfix"></div>
                        <br />
                        <div>
                            <div class="logo-gris">
                                <asset:image src="logo-grey.png" style="max-height:100%; max-width:100%;"/>

                            </div>

                            <p>©2016 Todos los derechos reservados. Lokerz Sistema de Horarios. Privacy and Terms</p>
                        </div>
                    </div>
                </g:form>
                <!-- form -->
            </section>
            <!-- content -->
        </div>

    </div>
</div>

    <script>
        function checkPassword(){
            if($('#password1').val()==$('#password2').val()){
                $('#password2').css('background-color','#66cc66');
                $('#mensaje').html('Contraseñas Coinciden');
                $('#submit').prop('disabled',false);
            }else{
                $('#password2').css('background-color','#ff6666');
                $('#mensaje').html('Contraseñas no Coinciden');
                $('#submit').prop('disabled',true);
            }
        }
    </script>

</body>
</html>