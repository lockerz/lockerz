<%--
  Created by IntelliJ IDEA.
  User: JUAN
  Date: 29/05/2016
  Time: 19:07
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>

    <title>Cambiar Password</title>
</head>
<asset:stylesheet href="bootstrap.min.css" />
<asset:stylesheet src="/css/font-awesome.min.css" />
<asset:stylesheet src="animate.min.css" />
<asset:stylesheet src="custom.css" />
<asset:stylesheet src="estilo.css" />
<asset:stylesheet src="maps/jquery-jvectormap-2.0.3.css" />
<asset:stylesheet src="icheck/flat/green.css" />
<asset:stylesheet src="floatexamples.css" />
<asset:javascript src="jquery.min.js"/>


<body style="background:#F7F7F7;">

<div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper">

        <div id="login" class="animate form">
            <g:if test='${flash.message}'>
                <div class="alert alert-info">${flash.message}</div>
            </g:if>
            <section class="login_content">
                <g:form action="restarPassword" method="POST">
                    <h1>Login</h1>
                    <div>

                        <input type="email" class="form-control" placeholder="Correo" required name="username" id="username"/>
                    </div>

                    <div>
                        <input type="submit" value="Entrar" class="btn btn-default submit" id="submit" >
                    </div>
                    <div class="clearfix"></div>
                    <div class="separator">


                        <div class="clearfix"></div>
                        <br />
                        <div>
                            <div class="logo-gris">
                                <asset:image src="logo-grey.png" style="max-height:100%; max-width:100%;"/>

                            </div>

                            <p>©2016 Todos los derechos reservados. Lokerz Sistema de Horarios. Privacy and Terms</p>
                        </div>
                    </div>
                </g:form>
            <!-- form -->
            </section>
            <!-- content -->
        </div>

    </div>
</div>



</body>
</html>