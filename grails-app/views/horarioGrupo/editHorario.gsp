<%--
  Created by IntelliJ IDEA.
  User: JUAN
  Date: 24/05/2016
  Time: 10:24
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Generación de horarios<small></small></h3>
        </div>

    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Generación de horario</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <g:if test="${flash.message}">
                    <div class="alert alert-dismissible alert-success">
                        <strong>${flash.message}</strong>
                    </div>
                </g:if>
                <div class="x_content">
                    <div class="col-md-12">
                        <div class="row materias">
                            <div class="form-group">
                                <label class="col-lg-1 control-label">Licenciatura:</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control"  placeholder="${grupo.licenciatura.nombre}" disabled>
                                </div>
                                <label class="col-lg-1 control-label">Grupo:</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control"  placeholder="${grupo.nombre}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row materias">
                            <div class="form-group">
                                <label class="col-lg-1 control-label">Periodo:</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control"  placeholder="${convocatoria.periodo} ${convocatoria.anio}" disabled>
                                </div>
                                <label class="col-lg-1 control-label">Aula:</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control"  placeholder="${grupo.aula.nombre}" disabled>
                                </div>
                            </div>
                        </div>

                    </div>
                    <g:form action="updateHorario" method="PUT">
                        <input type="hidden" name="grupo.id" value="${grupo.id}">
                    <!-- start project list -->
                    <div class="col-md-12 col-xs-12">
                        <table class="table table-bordered projects text-center disponibilidad">
                            <thead>
                            <tr>
                                <th class="col-md-2 text-center">Módulo</th>
                                <th class="col-md-2 text-center">Lunes</th>
                                <th class="col-md-2 text-center">Martes</th>
                                <th class="col-md-2 text-center">Miércoles</th>
                                <th class="col-md-2 text-center">Jueves</th>
                                <th class="col-md-2 text-center">Viernes</th>
                            </tr>
                            </thead>
                            <tbody>
                            <%id=0%>
                            <% inicio=grupo.turno=="Matutino"?7:14%>
                            <% fin=grupo.turno=="Matutino"?13:20%>
                            <g:each in="${(inicio..fin)}" var="hora">
                                <tr>
                                    <td>${hora<10?'0':''}${hora}:00-${(hora+1)<10?'0':''}${hora+1}:00</td>
                                    <td id="tdL${hora}" onclick="" ><g:select class="form-control" name="grupo.horario[${id}].materia" from="${asignaciones.materia}" id="selectL${hora}" noSelection="${['null':'No Asignada']}" optionValue="nombre" optionKey="id" value="${grupo.horario.find{it.indice==("L"+hora)}.materia?.id}" onclick="ajaxListaMateriasDisponibles(this, 'L${hora}',${grupo.id});"/></td>
                                    <input type="hidden" name="grupo.horario[${id}].id" value="${grupo.horario.find{it.indice==("L"+hora)}.id}">

                                    <%id++%>
                                    <td id="tdM${hora}" onclick="" ><g:select class="form-control" name="grupo.horario[${id}].materia" from="${asignaciones.materia}" id="selectM${hora}" noSelection="${['null':'No Asignada']}" optionValue="nombre" optionKey="id" value="${grupo.horario.find{it.indice==("M"+hora)}.materia?.id}" onclick="ajaxListaMateriasDisponibles(this, 'M${hora}',${grupo.id});"/></td>
                                    <input type="hidden" name="grupo.horario[${id}].id" value="${grupo.horario.find{it.indice==("M"+hora)}.id}">

                                    <%id++%>
                                    <td id="tdX${hora}" onclick=""><g:select class="form-control" name="grupo.horario[${id}].materia" from="${asignaciones.materia}" id="selectX${hora}" noSelection="${['null':'No Asignada']}" optionValue="nombre" optionKey="id" value="${grupo.horario.find{it.indice==("X"+hora)}.materia?.id}" onclick="ajaxListaMateriasDisponibles(this, 'X${hora}',${grupo.id});"/></td>
                                    <input type="hidden" name="grupo.horario[${id}].id" value="${grupo.horario.find{it.indice==("X"+hora)}.id}">

                                    <%id++%>
                                    <td id="tdJ${hora}" onclick=""><g:select class="form-control" name="grupo.horario[${id}].materia" from="${asignaciones.materia}" id="selectJ${hora}" noSelection="${['null':'No Asignada']}" optionValue="nombre" optionKey="id" value="${grupo.horario.find{it.indice==("J"+hora)}.materia?.id}" onclick="ajaxListaMateriasDisponibles(this, 'J${hora}',${grupo.id});"/></td>
                                    <input type="hidden" name="grupo.horario[${id}].id" value="${grupo.horario.find{it.indice==("J"+hora)}.id}">

                                    <%id++%>
                                    <td id="tdV${hora}" onclick=""><g:select class="form-control" name="grupo.horario[${id}].materia" from="${asignaciones.materia}" id="selectV${hora}" noSelection="${['null':'No Asignada']}" optionValue="nombre" optionKey="id" value="${grupo.horario.find{it.indice==("V"+hora)}.materia?.id}" onclick="ajaxListaMateriasDisponibles(this, 'V${hora}',${grupo.id});"/></td>
                                    <input type="hidden" name="grupo.horario[${id}].id" value="${grupo.horario.find{it.indice==("V"+hora)}.id}">

                                    <%id++%>
                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-12 col-lg-12">
                        <table class="table table-bordered projects">
                            <thead>
                            <tr class="info">
                                <th>Codigo</th>
                                <th>Materia</th>
                                <th>Profesor Asignado</th>
                                <th>Horas</th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${asignaciones}" var="${asignacion}">
                                <tr>
                                    <td>${asignacion.materia.codigo}</td>
                                    <td>${asignacion.materia.nombre}</td>
                                    <td>${asignacion.profesor.nombres} ${asignacion.profesor.apellidos}</td>
                                    <td>${asignacion.materia.numeroHoras}</td>
                                </tr>

                            </g:each>

                            </tbody>
                        </table>
                    </div>
                    <!-- end project list -->

                    <div class="row text-right">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-12 col-xs-12">
                                    <g:link action="index" class="btn btn-default">Regresar</g:link>
                                    <button type="submit" class="btn btn-primary">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </g:form>
                </div>
            </div>
        </div>
    </div>

</div>
<content tag="javascript">
    <script>
        function ajaxListaMateriasDisponibles(selectId, indice, grupo){
            console.log('hola');
            $.ajax({
                url: "../ajaxListaMateriasDisponibles",
                type: "POST",
                data: {
                    'indice' : indice,
                    'grupo' : grupo
                },
                dataType: "html",
                success: function(resultados){
                    $("#"+selectId.id).html(resultados);
                    $("#"+selectId.id).prop('onclick',null).off('click');
                }
            });

        }
    </script>
</content>
</body>
</html>