<%--
  Created by IntelliJ IDEA.
  User: JUAN
  Date: 24/05/2016
  Time: 9:48
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Grupos <small></small></h3>
        </div>

    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Lista de Horarios</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <!-- start project list -->
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th>Grupo</th>
                            <th>Licenciatura</th>
                            <th>Generar Horario</th>
                            <th>Ver Horario </th>
                        </tr>
                        </thead>
                        <tbody>
                            <g:each in="${grupos}" var="grupo">
                                <tr>
                                    <td><h4>${grupo.nombre}</h4></td>
                                    <td><h4>${grupo.licenciatura.nombre}</h4></td>
                                    <td><g:link action="editHorario" id="${grupo.id}" class="btn btn-info btn"><i class="fa fa-calendar"></i> Generar Horario</g:link></td>
                                    <td><g:link action="verHorario" id="${grupo.id}" class="btn btn-success btn"><i class="fa fa-eye"></i> Visualizar Horario</g:link> </td>
                                </tr>
                            </g:each>
                        </tbody>
                    </table>
                    <!-- end project list -->

                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>