<!DOCTYPE html>
<html>
    <head>

        <title>Lista Licenciaturas</title>
    </head>
    <body>
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Licenciaturas <small></small></h3>
            </div>


        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Lista de Licenciaturas</h2>

                        <div class="clearfix"></div>
                    </div>
                    <g:if test="${flash.message}">
                        <div class="alert alert-dismissible alert-warning">
                            <p>${flash.message}</p>
                        </div>
                    </g:if>
                    <div class="x_content">

                        <!-- start project list -->
                        <table class="table table-striped table-hover projects">
                            <thead>
                            <tr>
                                <th style="width:50%;">Nombre</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${licenciaturas?}" var="licenciatura">
                                <tr>
                                    <td>
                                        <h4>
                                            <g:link action="show" id="${licenciatura.id}">
                                                ${licenciatura.nombre}
                                            </g:link>
                                        </h4>
                                    </td>
                                    <td>
                                        <g:form resource="${licenciatura}" method="DELETE">
                                            <g:link action="edit" id="${licenciatura.id}" class="btn btn-success"> Editar</g:link>
                                            <input class="btn btn-danger" type="submit" value="Eliminar" onclick="return confirm('Esta seguro de eliminar este registro?');" />
                                        </g:form>
                                    </td>
                                </tr>
                            </g:each>

                            </tbody>
                        </table>
                        <!-- end project list -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>