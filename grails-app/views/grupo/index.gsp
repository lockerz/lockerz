<!DOCTYPE html>
<html>
    <head>

        <title>Lista Grupos</title>
        <asset:stylesheet src="datatables/jquery.dataTables.min.css" />
        <asset:stylesheet src="datatables/buttons.bootstrap.min.css" />
        <asset:stylesheet src="datatables/fixedHeader.bootstrap.min.css" />
        <asset:stylesheet src="datatables/responsive.bootstrap.min.css" />
        <asset:stylesheet src="datatables/scroller.bootstrap.min.css" />
    </head>
    <body>
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Grupos <small></small></h3>
            </div>


        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Lista de Grupos</h2>

                        <div class="clearfix"></div>
                    </div>
                    <g:if test="${flash.message}">
                        <div class="alert alert-dismissible alert-warning">
                            <p>${flash.message}</p>
                        </div>
                    </g:if>
                    <div class="x_content">

                        <!-- start project list -->
                        <table id="datatable" class="table table-striped table-hover projects">
                            <thead>
                            <tr>
                                <th style="width:25%;">Nombre</th>
                                <th>Licenciatura</th>
                                <th>Aula</th>
                                <th>Activo</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${grupos?}" var="grupo">
                                <tr>
                                    <td>
                                        <h4>
                                            <g:link action="show" id="${grupo.id}">
                                                ${grupo.nombre}
                                            </g:link>
                                        </h4>
                                    </td>
                                    <td>
                                        <h4>${grupo.licenciatura.nombre}</h4>
                                    </td>
                                    <td>
                                        <h4>${grupo.aula.nombre}</h4>
                                    </td>
                                    <td>
                                        <g:if test="${grupo.activo}">
                                            <h4><span class="label label-success">Activo</span></h4>
                                        </g:if>
                                        <g:else>
                                            <h4><span class="label label-danger">Inactivo</span></h4>
                                        </g:else>

                                    </td>
                                    <td>
                                        <g:form resource="${grupo}" method="DELETE">
                                            <g:link action="edit" id="${grupo.id}" class="btn btn-success"> Editar</g:link>
                                            <input class="btn btn-danger" type="submit" value="Eliminar" onclick="return confirm('Esta seguro de eliminar este registro?');" />
                                        </g:form>
                                    </td>
                                </tr>
                            </g:each>

                            </tbody>
                        </table>
                        <!-- end project list -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <content tag="javascript">
        <asset:javascript src="datatables/jquery.dataTables.min.js"/>
        <asset:javascript src="datatables/dataTables.bootstrap.js"/>
        <asset:javascript src="datatables/dataTables.buttons.min.js"/>
        <asset:javascript src="datatables/buttons.bootstrap.min.js"/>
        <asset:javascript src="datatables/jszip.min.js"/>
        <asset:javascript src="datatables/pdfmake.min.js"/>
        <asset:javascript src="datatables/vfs_fonts.js"/>
        <asset:javascript src="datatables/buttons.html5.min.js"/>
        <asset:javascript src="datatables/buttons.print.min.js"/>
        <asset:javascript src="datatables/dataTables.fixedHeader.min.js"/>
        <asset:javascript src="datatables/dataTables.keyTable.min.js"/>
        <asset:javascript src="datatables/dataTables.responsive.min.js"/>
        <asset:javascript src="datatables/responsive.bootstrap.min.js"/>
        <asset:javascript src="datatables/dataTables.scroller.min.js"/>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').dataTable();
                $('#datatable-keytable').DataTable({
                    keys: true
                });
                $('#datatable-responsive').DataTable();
                $('#datatable-scroller').DataTable({
                    ajax: "js/datatables/json/scroller-demo.json",
                    deferRender: true,
                    scrollY: 380,
                    scrollCollapse: true,
                    scroller: true
                });
                var table = $('#datatable-fixed-header').DataTable({
                    fixedHeader: true
                });
            });
            TableManageButtons.init();
        </script>
    </content>
    </body>
</html>