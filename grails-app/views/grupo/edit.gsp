<!DOCTYPE html>
<html>
    <head>
        <title>Editar Grupo</title>
    </head>
    <body>
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Grupos
                </h3>
            </div>

        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Alta de grupo </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <g:hasErrors bean="${this.grupo}">
                            <ul class="errors" role="alert">
                                <g:eachError bean="${this.grupo}" var="error">
                                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                                </g:eachError>
                            </ul>
                        </g:hasErrors>

                        <g:form action="update" method="PUT" class="form-horizontal form-label-left" novalidate="true">
                            <input type="hidden" name="grupo.id" value="${grupo.id}">
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">Nombre <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="nombre" value="${grupo.nombre}" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="1" name="grupo.nombre" required="required" type="text">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Semestre <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <g:select name="grupo.semestre" from="${1..9}" value="${grupo.semestre}" class="form-control col-md-7 col-xs-12" />

                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Licenciatura <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <g:select name="grupo.licenciatura" from="${licenciaturas}" optionValue="nombre" optionKey="id" value="${grupo.licenciatura?.id}" class="form-control col-md-7 col-xs-12" />
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Turno <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <g:select name="grupo.turno" from="${['Matutino','Vespertino']}"  value="${grupo.turno}" class="form-control col-md-7 col-xs-12" />
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Aula <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <g:select name="grupo.aula" from="${aulas}" optionValue="nombre" optionKey="id" value="${grupo.aula?.id}" class="form-control col-md-7 col-xs-12" />
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="activo">Activo <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="hidden" name="grupo._activo"/>
                                    <input type="checkbox" id="activo" ${grupo.activo?"checked":""} name="grupo.activo"grupo class="form-control col-md-7 col-xs-12" >
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                    <g:link action="index" class="btn btn-info">Cancelar</g:link>
                                    <button id="send" type="submit" class="btn btn-success">Aceptar</button>
                                </div>
                            </div>
                        </g:form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <content tag="javascript">
        <asset:javascript src="validator/validator.js"/>

        <script>
            // initialize the validator function
            validator.message['date'] = 'not a real date';

            // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
            $('form')
                    .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                    .on('change', 'select.required', validator.checkField)
                    .on('keypress', 'input[required][pattern]', validator.keypress);

            $('.multi.required')
                    .on('keyup blur', 'input', function() {
                        validator.checkField.apply($(this).siblings().last()[0]);
                    });

            // bind the validation to the form submit event
            //$('#send').click('submit');//.prop('disabled', true);

            $('form').submit(function(e) {
                e.preventDefault();
                var submit = true;
                // evaluate the form using generic validaing
                if (!validator.checkAll($(this))) {
                    submit = false;
                }

                if (submit)
                    this.submit();
                return false;
            });

            /* FOR DEMO ONLY */
            $('#vfields').change(function() {
                $('form').toggleClass('mode2');
            }).prop('checked', false);

            $('#alerts').change(function() {
                validator.defaults.alerts = (this.checked) ? false : true;
                if (this.checked)
                    $('form .alert').remove();
            }).prop('checked', false);
        </script>

    </content>
    </body>
</html>
