<!DOCTYPE html>
<html>
    <head>
        <title>Lista Profesores</title>
        <asset:stylesheet src="datatables/jquery.dataTables.min.css" />
        <asset:stylesheet src="datatables/buttons.bootstrap.min.css" />
        <asset:stylesheet src="datatables/fixedHeader.bootstrap.min.css" />
        <asset:stylesheet src="datatables/responsive.bootstrap.min.css" />
        <asset:stylesheet src="datatables/scroller.bootstrap.min.css" />
    </head>
    <body>
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Profesores <small></small></h3>
            </div>


        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Lista de Profesores</h2>

                        <div class="clearfix"></div>
                    </div>
                    <g:if test="${flash.message}">
                        <div class="alert alert-dismissible alert-warning">
                            <p>${flash.message}</p>
                        </div>
                    </g:if>
                    <div class="x_content">

                        <!-- start project list -->
                        <table id="datatable" class="table table-striped table-hover projects">
                            <thead>
                            <tr>
                                <th></th>
                                <th style="width:20%;">Nombres</th>
                                <th style="width:20%;">Apellidos</th>
                                <th>Estatus</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${profesores?}" var="profesor">
                                <tr>
                                    <td>
                                        <g:img dir="images" file="${profesor.avatar}" class="img-circle fotoListaPerfil"/>
                                       </td>
                                    <td>
                                        <h4>
                                            <g:link action="show" id="${profesor.id}">
                                                ${profesor.nombres}
                                            </g:link>
                                        </h4>
                                    </td>
                                    <td>
                                        <h4>${profesor.apellidos}</h4>
                                    </td>
                                    <td>
                                        <g:if test="${profesor.activo}">
                                            <h4><span class="label label-success">Activo</span></h4>
                                        </g:if>
                                        <g:else>
                                            <h4><span class="label label-danger">Inactivo</span></h4>
                                        </g:else>

                                    </td>
                                    <td>
                                        <g:form resource="${profesor}" method="DELETE">
                                            <g:link action="edit" id="${profesor.id}" class="btn btn-success"> Editar</g:link>
                                            <button onclick="event.preventDefault();ajaxEmail(${profesor.id})" class="btn btn-primary">Enviar Credenciales</button>
                                            <input class="btn btn-danger" type="submit" value="Eliminar" onclick="return confirm('Esta seguro de eliminar este registro?');" />
                                        </g:form>
                                    </td>
                                </tr>
                            </g:each>

                            </tbody>
                        </table>
                        <!-- end project list -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <content tag="javascript">
        <asset:javascript src="datatables/jquery.dataTables.min.js"/>
        <asset:javascript src="datatables/dataTables.bootstrap.js"/>
        <asset:javascript src="datatables/dataTables.buttons.min.js"/>
        <asset:javascript src="datatables/buttons.bootstrap.min.js"/>
        <asset:javascript src="datatables/jszip.min.js"/>
        <asset:javascript src="datatables/pdfmake.min.js"/>
        <asset:javascript src="datatables/vfs_fonts.js"/>
        <asset:javascript src="datatables/buttons.html5.min.js"/>
        <asset:javascript src="datatables/buttons.print.min.js"/>
        <asset:javascript src="datatables/dataTables.fixedHeader.min.js"/>
        <asset:javascript src="datatables/dataTables.keyTable.min.js"/>
        <asset:javascript src="datatables/dataTables.responsive.min.js"/>
        <asset:javascript src="datatables/responsive.bootstrap.min.js"/>
        <asset:javascript src="datatables/dataTables.scroller.min.js"/>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').dataTable({
                    "language": {
                      url:"//cdn.datatables.net/plug-ins/1.10.12/i18n/Spanish.json"
                    }
                });
                $('#datatable-keytable').DataTable({
                    keys: true
                });
                $('#datatable-responsive').DataTable();
                $('#datatable-scroller').DataTable({
                    ajax: "js/datatables/json/scroller-demo.json",
                    deferRender: true,
                    scrollY: 380,
                    scrollCollapse: true,
                    scroller: true
                });
                var table = $('#datatable-fixed-header').DataTable({
                    fixedHeader: true
                });
            });
            TableManageButtons.init();
        </script>

        
        <script>

            function ajaxEmail(id){
                $.ajax({
                    url: "../profesor/ajaxSendUserMail",
                    type: "POST",
                    data: {
                        'id' : id
                    },

                    dataType: "html",
                    beforeSend: function(){
                      ohSnap('Enviando...',{'duration':'2000', color:'green'});
                    },
                    success: function(mensaje){
                        ohSnap(mensaje, {'duration':'2000', color:'blue', icon: 'fa fa-envelope-o'});
                    }
                });
            }
        </script>
    </content>
    </body>
</html>
