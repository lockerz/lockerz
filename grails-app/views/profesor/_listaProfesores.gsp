<!-- start project list -->
<table id="datatable" class="table table-striped table-hover projects">
    <thead>
    <tr>
        <th></th>
        <th style="width:20%;">Nombres</th>
        <th style="width:20%;">Apellidos</th>
        <th>Estatus</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${profesores?}" var="profesor">
        <tr>
            <td>
                <g:img dir="images" file="${profesor.avatar}" class="img-circle fotoListaPerfil"/>
            </td>
            <td>
                <h4>
                    <g:link action="show" id="${profesor.id}">
                        ${profesor.nombres}
                    </g:link>
                </h4>
            </td>
            <td>
                <h4>${profesor.apellidos}</h4>
            </td>
            <td>
                <g:if test="${profesor.activo}">
                    <h4><span class="label label-success">Activo</span></h4>
                </g:if>
                <g:else>
                    <h4><span class="label label-danger">Inactivo</span></h4>
                </g:else>

            </td>
            <td>
                    <button onclick="event.preventDefault();ajaxActivo(${profesor.id})" class="btn btn-primary">Cambiar estado</button>
            </td>
        </tr>
    </g:each>

    </tbody>
</table>
<!-- end project list -->
