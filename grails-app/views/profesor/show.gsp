<!DOCTYPE html>
<html>
    <head>
        <title>Profesor ${profesor.apellidos}</title>
    </head>
    <body>
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Profesores
                </h3>
            </div>

        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Detalle de profesor </h2>
                        <div class="clearfix"></div>
                    </div>
                    <g:if test="${flash.message}">
                        <div class="alert alert-dismissible alert-success">
                            <strong>Se ha dado de alta con éxito el siguiente profesor:</strong>
                        </div>
                    </g:if>
                    <div class="x_content">
                        <div class="form-horizontal form-label-left">

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Nombres <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label class="form-control col-md-7 col-xs-12">${profesor.nombres}</label>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Apellidos <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label class="form-control col-md-7 col-xs-12">${profesor.apellidos}</label>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Correo Electrónico <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label class="form-control col-md-7 col-xs-12">${profesor.correoElectronico}</label>
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Cédula Profesional <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label class="form-control col-md-7 col-xs-12">${profesor.cedula}</label>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Teléfono <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label class="form-control col-md-7 col-xs-12">${profesor.telefono}</label>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Fecha limite para subir disponibilidad <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label class="form-control col-md-7 col-xs-12">${(profesor.fechaCierreProfesor!=null)?profesor.fechaCierreProfesor:""}</label>

                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Activo <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label class="form-control col-md-7 col-xs-12">${profesor.activo?"Si":"no"}</label>

                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                    <!--<button type="submit" class="btn btn-primary">Cancelar</button>-->

                                    <g:form resource="${this.profesor}" method="DELETE">
                                        <g:link action="edit" id="${profesor.id}" class="btn btn-success"> Editar</g:link>
                                        <g:link action="index" class="btn btn-info">Regresar</g:link>
                                        <input class="btn btn-danger" type="submit" value="Eliminar" onclick="return confirm('Esta seguro de eliminar este registro?');" />
                                    </g:form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <!--Carga de disponibilidad del maestro asignado-->
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Disponibilidad del Profesor</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <g:if test="${disponibilidad.size()>0}">
                    <div class="x_content" id="divDisponibilidad">
                        <div class="col-md-10 col-md-offset-1">
                            <table class="table table-bordered projects text-center disponibilidad">
                                <thead>
                                <tr>
                                    <th class="col-md-2 text-center">Módulo</th>
                                    <th class="col-md-2 text-center">Lunes</th>
                                    <th class="col-md-2 text-center">Martes</th>
                                    <th class="col-md-2 text-center">Miércoles</th>
                                    <th class="col-md-2 text-center">Jueves</th>
                                    <th class="col-md-2 text-center">Viernes</th>
                                </tr>
                                </thead>
                                <tbody>
                                <%id=0%>
                                <g:each in="${(7..20)}" var="hora">
                                    <tr >
                                        <td>${hora<10?'0':''}${hora}:00-${(hora+1)<10?'0':''}${hora+1}:00</td>
                                        <%materia=disponibilidad.find{it.indice==("L"+hora)}.materia%>
                                        <td id="tdL${hora}" style="color:#fff;" class="${disponibilidad.find{it.indice==("L"+hora)}.disponible?'disponible':''}">${materia==null?'':materia.nombre}</td>
                                        <%id++%>
                                        <%materia=disponibilidad.find{it.indice==("M"+hora)}.materia%>
                                        <td id="tdM${hora}" style="color:#fff;" class="${disponibilidad.find{it.indice==("M"+hora)}.disponible?'disponible':''}">${materia==null?'':materia.nombre}</td>
                                        <%id++%>
                                        <%materia=disponibilidad.find{it.indice==("X"+hora)}.materia%>
                                        <td id="tdX${hora}" style="color:#fff;" class="${disponibilidad.find{it.indice==("X"+hora)}.disponible?'disponible':''}">${materia==null?'':materia.nombre}</td>
                                        <%id++%>
                                        <%materia=disponibilidad.find{it.indice==("J"+hora)}.materia%>
                                        <td id="tdJ${hora}" style="color:#fff;" class="${disponibilidad.find{it.indice==("J"+hora)}.disponible?'disponible':''}">${materia==null?'':materia.nombre}</td>
                                        <%id++%>
                                        <%materia=disponibilidad.find{it.indice==("V"+hora)}.materia%>
                                        <td id="tdV${hora}" style="color:#fff;" class="${disponibilidad.find{it.indice==("V"+hora)}.disponible?'disponible':''}">${materia==null?'':materia.nombre}</td>
                                        <%id++%>
                                    </tr>
                                </g:each>
                                </tbody>
                            </table>
                        </div>
                        <!-- end project list -->

                        <div class="row text-right">
                            <div class="col-md-12">
                                <asset:image src="icondisp.png" style="height:30px; display:inline-block;"/>
                                <h5 style="display:inline-block;"><em>Horario disponible para impartir clase</em></h5>
                            </div>
                        </div>


                        <div class="row materias">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-12 col-xs-12">Lista de Materias</label>
                                    <div class="col-sm-12 col-xs-12">
                                        <table  class="table">
                                            <tbody id="listaPreferencia">
                                            <g:each in="${preferencias}" var="row" >
                                                <tr id="materia${row.materia.id}">
                                                    <td>${row.materia.nombre}<input type="hidden" name="preferencias[]" value="${row.materia.id}"></td>
                                                    <td>${row.materia.licenciatura.nombre}</td>
                                                    <td>${row.materia.semestre} semestre</td>
                                                    <td class="text-center"><span class=" btn btn-danger badge" onclick="$('#${row.materia.id}').remove()"><i class="fa fa-times"></i></span></td>
                                                </tr>
                                            </g:each>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="x_title">
                                    <h2>Comentarios del Profesor</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <p>
                                        ${profesor.comentario}
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                    </g:if>
                </div>
            </div>
        </div>




    </div>

    </body>
</html>
