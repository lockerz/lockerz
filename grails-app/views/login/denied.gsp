<%--
  Created by IntelliJ IDEA.
  User: JUAN
  Date: 05/06/2016
  Time: 12:58
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>No Autorizado</title>
    <asset:stylesheet href="bootstrap.min.css" />
    <asset:stylesheet src="/css/font-awesome.min.css" />
    <asset:stylesheet src="animate.min.css" />
    <asset:stylesheet src="custom.css" />
    <asset:stylesheet src="estilo.css" />
    <asset:stylesheet src="maps/jquery-jvectormap-2.0.3.css" />
    <asset:stylesheet src="icheck/flat/green.css" />
    <asset:stylesheet src="floatexamples.css" />
    <asset:javascript src="jquery.min.js"/>
</head>

<body class="nav-md">

<div class="container body">

    <div class="main_container">

        <!-- page content -->
        <div class="col-md-12">
            <div class="col-middle">
                <div class="text-center text-center">
                    <h1 class="error-number">403</h1>
                    <h2>Lo sentimos, pero no tienes acceso a esta página</h2>
                    <p>No tienes acceso a la página que deseas acceder <a href="mailto:soporte@lockerz.mx">¿Tienes dudas acerca de esto? Repórtalo</a>
                    </p>
                    <div class="mid_center">
                        <h3>Continuar en la página</h3>
                        <g:link controller="principal">Regresar al sitio</g:link>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

    </div>
    <!-- footer content -->
</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<asset:javascript src="bootstrap.min.js"/>



<asset:javascript src="custom.js"/>
<!-- pace -->
</body>
</html>