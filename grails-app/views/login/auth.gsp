<%--
  Created by IntelliJ IDEA.
  User: JUAN
  Date: 04/05/2016
  Time: 12:50
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>

    <title>Login</title>
    <asset:stylesheet href="bootstrap.min.css" />
    <asset:stylesheet src="/css/font-awesome.min.css" />
    <asset:stylesheet src="animate.min.css" />
    <asset:stylesheet src="custom.css" />
    <asset:stylesheet src="estilo.css" />
    <asset:stylesheet src="maps/jquery-jvectormap-2.0.3.css" />
    <asset:stylesheet src="icheck/flat/green.css" />
    <asset:stylesheet src="floatexamples.css" />
    <asset:javascript src="jquery.min.js"/>
</head>



<body style="background:#F7F7F7;">

<div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper">
        <g:if test='${flash.message}'>
            <div class="alert-info">${flash.message}</div>
        </g:if>
        <div id="login" class="animate form">
            <section class="login_content">
                <form action="${postUrl ?: '/login/authenticate'}" method="POST" id="loginForm" class="cssform" autocomplete="off">
                    <h1>Login</h1>
                    <div>

                        <input type="text" class="form-control" placeholder="Correo" required="" name="${usernameParameter ?: 'username'}" id="username"/>
                    </div>
                    <div>
                        <input type="password" class="form-control" placeholder="Contraseña" required="" name="${passwordParameter ?: 'password'}" id="password"/>
                    </div>
                    <div>
                        <p id="remember_me_holder">
                            <input type="checkbox" class="chk" name="${rememberMeParameter ?: 'remember-me'}" id="remember_me" <g:if test='${hasCookie}'>checked="checked"</g:if>/>
                            <label for="remember_me">Recordarme</label>
                        </p>
                    </div>
                    <div>
                        <g:link controller="accounts" action="recoverPassword">
                        Contraseña olvidada
                        </g:link>
                    </div>
                    <div>
                        <input type="submit" value="Entrar" class="btn btn-default submit">
                    </div>
                    <div class="clearfix"></div>
                    <div class="separator">


                        <div class="clearfix"></div>
                        <br />
                        <div>
                            <div class="logo-gris">
                                <asset:image src="logo-grey.png" style="max-height:100%; max-width:100%;"/>

                            </div>

                            <p>©2016 Todos los derechos reservados. Lokerz Sistema de Horarios. Privacy and Terms</p>
                        </div>
                    </div>
                </form>
                <!-- form -->
            </section>
            <!-- content -->
        </div>

    </div>
</div>
<content tag="javascript">
    <script>
        (function() {
            document.forms['loginForm'].elements['${usernameParameter ?: 'username'}'].focus();
        })();
    </script>
</content>
</body>
</html>