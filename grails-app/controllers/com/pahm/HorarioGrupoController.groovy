package com.pahm

class HorarioGrupoController {
    static layout = "administrador"

    def index() {
        def grupos= Grupo.listOrderByNombre()
        respond grupos:grupos
    }

    def editHorario(Grupo grupo){
        def convocatoria=Convocatoria.findByActual(true)
        if(convocatoria==null){
            redirect(action: 'index')
            return
        }
        def asignaciones=MateriaProfesorGrupo.findAllByGrupo(grupo)
        if(asignaciones.size()==0){
            redirect(action: 'index')
            return
        }
        int inicio=grupo.turno=="Matutino"?7:14
        int fin=grupo.turno=="Matutino"?14:21
        if(grupo.horario.size()==0){
            for (int i =inicio ; i < fin; i++) {
                grupo.addToHorario(new HorarioGrupo(indice: 'L'+i ))
                grupo.addToHorario(new HorarioGrupo(indice: 'M'+i ))
                grupo.addToHorario(new HorarioGrupo(indice: 'X'+i ))
                grupo.addToHorario(new HorarioGrupo(indice: 'J'+i ))
                grupo.addToHorario(new HorarioGrupo(indice: 'V'+i ))
            }
        }


        grupo.save flush:true
        respond grupo: grupo, convocatoria: convocatoria, asignaciones: asignaciones

    }

    def updateHorario(Grupo grupo){
        grupo.save flush:true
        flash.message='El horario ha sido guardado.'
        redirect action: 'editHorario', id: grupo.id
    }

    def ajaxListaMateriasDisponibles(){

        def grupo=Grupo.get(params.grupo)
        def indice= params.indice
        def criteria=DisponibilidadProfesor.createCriteria()
        def profesores=DisponibilidadProfesor.withCriteria{
            eq("indice",indice)
            eq("disponible",true)
            or{
                isNull("materia")
            }

        }.profesor
        def materias=new ArrayList<Materia>()
        if(profesores.size()>0){
            materias=MateriaProfesorGrupo.findAllByProfesorInListAndGrupo(profesores,grupo).materia
        }
        def materia=HorarioGrupo.findByIndiceAndGrupo(indice,grupo).materia
        if(materia!=null){
            materias.push(materia)
        }
        render template: "selectMateriasDisponibles", model: [materias: materias], contentType: 'text/plain'
    }

    def verHorario(Grupo grupo){

    }
}
