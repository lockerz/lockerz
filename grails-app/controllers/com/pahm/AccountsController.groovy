package com.pahm

import org.apache.commons.lang.RandomStringUtils

class AccountsController {
    def passwordEncoder
    def index() {
        redirect(action: 'editPassword')
    }

    def editPassword(){
        flash.message='Por razones de seguridad, es necesario que cambies tu contraseña provisional'
        respond view:'editPassword'
    }

    def updatePassword(){
        def user = SecUser.findByUsername(params.username)
        if(user==null){
            flash.message = 'Correo invalido, intenta de nuevo'
            respond flash.message, view:'editPassword'
            return
        }
        if (!passwordEncoder.isPasswordValid(user.password, params.currentPassword, null /*salt*/)) {
            flash.message = 'Contraseña provisional incorrecta, intenta de nuevo'
            respond flash.message, view:'editPassword'
            return
        }
        if (params.currentPassword==params.confirmPassword){
            flash.message = 'No puedes usar tu contraseña provisional como contraseña, intenta de nuevo'
            respond flash.message, view:'editPassword'
            return
        }
        if (params.newPassword!=params.confirmPassword){
            flash.message = 'Por favor introduce tu contraseña provisional y una nueva contraseña'
            respond flash.message, view:'editPassword'
            return
        }
        user.password=params.newPassword
        user.passwordExpired=false
        user.save flush:true
        flash.message = 'Ya puede ingresar con su nueva contraseña'
        respond flash.message, view:'/login/auth'
    }

    def recoverPassword(){
        flash.message='Introduce tu correo electronico'
        respond view: 'recoverPassword'
    }

    def restarPassword(){
        def user = SecUser.findByUsername(params.username)
        if(user==null){
            flash.message = 'Correo invalido, intenta de nuevo'
            respond flash.message, view:'recoverPassword'
            return
        }
        String password=  RandomStringUtils.randomAlphanumeric(8)
        user.password=password
        user.passwordExpired= true
        user.save flush: true
        def profesor= Profesor.findByUsuario(user)
        if(profesor!=null){
            profesor.passwordInicial=password
            profesor.save flush: true
        }

        sendMail {
            to profesor.correoElectronico
            from "no-reply@lockerz.mx"
            subject "Cuenta de usuario"
            html view: "/emails/notificacionCuenta", model: [usuario: user.username, contrasenia:password]
        }

        flash.message='Una contraseña provisional se ha enviado a tu correo'
        respond flash.message, view:'/login/auth'
    }
}
