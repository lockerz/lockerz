package com.pahm

import static org.springframework.http.HttpStatus.*
import org.apache.commons.lang.RandomStringUtils;
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ProfesorController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    static layout = "administrador"

    def index(Integer max) {
        List<Profesor> profesores= Profesor.listOrderByApellidos()
        respond profesores:profesores
    }

    def show(Profesor profesor) {
        def disponibilidad= profesor.disponibilidad
        def preferencias= PreferenciaMaterias.findAllByProfesor(profesor)
        respond profesor:profesor, disponibilidad:disponibilidad, preferencias: preferencias
    }

    def create() {
        respond new Profesor(params)
    }

    @Transactional
    def save(Profesor profesor) {
        if (profesor == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }
        String password=  RandomStringUtils.randomAlphanumeric(8)
        profesor.passwordInicial= password
        profesor.usuario = new SecUser(username: profesor.correoElectronico, password: password, accountExpired: false, accountLocked: false, passwordExpired: true).save flush: true

        SecUserSecRole.create(profesor.usuario,SecRole.findByAuthority('ROLE_Maestro'),true)
        if (profesor.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond profesor.errors, view:'create'
            return
        }
        Convocatoria convocatoria = Convocatoria.findByActual(true)
        if(convocatoria!=null){
            profesor.fechaCierreProfesor=convocatoria.fechaCierre
        }
        profesor.activo=true
        profesor.save(flush:true);

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'profesor.label', default: 'Profesor'), profesor.id])
                redirect profesor
            }
            '*' { respond profesor, [status: CREATED] }
        }
    }

    def edit(Profesor profesor) {
        respond profesor
    }

    @Transactional
    def update(Profesor profesor) {
        if (profesor == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (profesor.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond profesor.errors, view:'edit'
            return
        }

        profesor.save flush:true
        profesor.usuario.accountLocked=!profesor.activo
        profesor.usuario.save flush: true
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'profesor.label', default: 'Profesor'), profesor.id])
                redirect profesor
            }
            '*'{ respond profesor, [status: OK] }
        }
    }

    @Transactional
    def delete(Profesor profesor) {

        if (profesor == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }
        SecUser user = SecUser.get(profesor.usuario.id)
        PreferenciaMaterias.where{profesor==profesor}.deleteAll();
        profesor.delete flush:true
        SecUserSecRole.removeAll(user)
        user.delete flush:true
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'profesor.label', default: 'Profesor'), profesor.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'profesor.label', default: 'Profesor'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def ajaxSendUserMail(Profesor profesor){
        sendMail {
            to profesor.correoElectronico
            from "no-reply@lockerz.mx"
            subject "Cuenta de usuario"
            html view: "/emails/notificacionCuenta", model: [usuario: profesor.correoElectronico, contrasenia:profesor.passwordInicial]
        }
        render 'mensaje enviado', contentType: 'text/plain'
    }
    @Transactional
    def ajaxToggleActivo(Profesor profesor){
        profesor.activo=!profesor.activo
        profesor.save flush: true
        profesor.usuario.accountLocked=!profesor.activo
        profesor.usuario.save flush: true
        def profesores = Profesor.listOrderByApellidos()
        render template: 'listaProfesores', model: [profesores: profesores], contentType: 'text/plain'
    }
}
