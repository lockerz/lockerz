package com.pahm

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class LicenciaturaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    static layout = "administrador"

    def index(Integer max) {
       List<Licenciatura> licenciaturas= Licenciatura.listOrderByNombre()
        respond licenciaturas:licenciaturas
    }

    def show(Licenciatura licenciatura) {
        respond licenciatura
    }

    def create() {
        respond new Licenciatura(params)
    }

    @Transactional
    def save(Licenciatura licenciatura) {
        if (licenciatura == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (licenciatura.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond licenciatura.errors, view:'create'
            return
        }
        licenciatura.activo=true
        licenciatura.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'licenciatura.label', default: 'Licenciatura'), licenciatura.id])
                redirect licenciatura
            }
            '*' { respond licenciatura, [status: CREATED] }
        }
    }

    def edit(Licenciatura licenciatura) {
        respond licenciatura
    }

    @Transactional
    def update(Licenciatura licenciatura) {
        if (licenciatura == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (licenciatura.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond licenciatura.errors, view:'edit'
            return
        }

        licenciatura.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'licenciatura.label', default: 'Licenciatura'), licenciatura.id])
                redirect licenciatura
            }
            '*'{ respond licenciatura, [status: OK] }
        }
    }

    @Transactional
    def delete(Licenciatura licenciatura) {

        if (licenciatura == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        licenciatura.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'licenciatura.label', default: 'Licenciatura'), licenciatura.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'licenciatura.label', default: 'Licenciatura'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
