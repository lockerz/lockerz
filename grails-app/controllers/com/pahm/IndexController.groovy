package com.pahm


class IndexController {
    def springSecurityService

    def index() {
        def usuario = springSecurityService.getPrincipal()
        def rol= usuario.getAuthorities().getAt(0)
        if(rol.authority.equals('ROLE_Administrador')){
            redirect(controller: 'administrador')
        }else{
            redirect(controller: 'usuario')
        }


    }
}
