package com.pahm

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MateriaController {
    static layout = "administrador"
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        List<Materia> materias= Materia.listOrderByLicenciatura()
        respond materias:materias
    }

    def show(Materia materia) {
        respond materia
    }

    def create() {
        List<Licenciatura> licenciaturas = Licenciatura.listOrderByNombre()
        respond licenciaturas:licenciaturas
    }

    @Transactional
    def save(Materia materia) {
        if (materia == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (materia.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond materia.errors, view:'create'
            return
        }
        materia.activo=true
        materia.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'materia.label', default: 'Materia'), materia.id])
                redirect materia
            }
            '*' { respond materia, [status: CREATED] }
        }
    }

    def edit(Materia materia) {
        List<Licenciatura> licenciaturas = Licenciatura.listOrderByNombre()
        respond materia:materia, licenciaturas: licenciaturas
    }

    @Transactional
    def update(Materia materia) {
        if (materia == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (materia.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond materia.errors, view:'edit'
            return
        }

        materia.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'materia.label', default: 'Materia'), materia.id])
                redirect materia
            }
            '*'{ respond materia, [status: OK] }
        }
    }

    @Transactional
    def delete(Materia materia) {

        if (materia == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        materia.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'materia.label', default: 'Materia'), materia.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'materia.label', default: 'Materia'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def archivos(Materia materia){
        respond materia: materia
    }

    @Transactional
    def uploadArchivos(Materia materia){
        
        def webrootDir = servletContext.getRealPath("/");
        def bibliografia= request.getFile('bibliografia')
        def temario= request.getFile('temario')


        if(bibliografia.empty && temario.empty){
            flash.message='Favor de seleccionar los archivos correspondientes e intentarlo de nuevo'
            redirect(action: 'archivos', id: materia.id)
            return
        }


        if(!bibliografia.empty && bibliografia.contentType.equals('application/vnd.openxmlformats-officedocument.wordprocessingml.document')){
            String nombre="materia/bibliografia_"+materia.codigo+materia.nombre+materia.id+'.docx'
            File fileDest = new File(webrootDir,"documents/"+nombre)
            bibliografia.transferTo(fileDest)
            materia.bibliografia=nombre
            materia.save flush: true
        }

        if(! bibliografia.contentType.equals('application/vnd.openxmlformats-officedocument.wordprocessingml.document')){
            flash.message='El archivo de bibliografia no es un archivo de Word (.docx)'
            redirect(action: 'archivos', id: materia.id)
            return
        }

        if(!temario.empty && temario.contentType.equals('application/vnd.openxmlformats-officedocument.wordprocessingml.document')){
            String nombre="materia/temario"+materia.codigo+materia.nombre+materia.id+'.docx'
            File fileDest = new File(webrootDir,"documents/"+nombre)
            temario.transferTo(fileDest)
            materia.temario=nombre
            materia.save flush: true
        }
        if(! temario.contentType.equals('application/vnd.openxmlformats-officedocument.wordprocessingml.document')){
            flash.message='El archivo de Temario no es un archivo de Word (.docx)'
            redirect(action: 'archivos', id: materia.id)
            return
        }

        flash.message='Los archivos han sido guardados con exito'
        redirect(action: 'archivos', id: materia.id)
    }
}
