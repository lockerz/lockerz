package com.pahm

import grails.transaction.Transactional


class UsuarioController {
    def springSecurityService
    def index() {
        respond profesor: Profesor.findByUsuario(SecUser.get(springSecurityService.getPrincipal().id))
    }

    def materiasAsignadas(){
        def convocatoria=Convocatoria.findByActual(true)
        def materiasAsignadas= MateriaProfesorGrupo.findAllByProfesorAndDocumentosDisponibles( Profesor.findByUsuario(SecUser.get(springSecurityService.getPrincipal().id)),true)

        respond profesor: Profesor.findByUsuario(SecUser.get(springSecurityService.getPrincipal().id)), convocatoria: convocatoria, materiasAsignadas: materiasAsignadas
    }

    def miPerfil(){
        respond profesor: Profesor.findByUsuario(SecUser.get(springSecurityService.getPrincipal().id))
    }

    def editPerfil(){
        respond profesor: Profesor.findByUsuario(SecUser.get(springSecurityService.getPrincipal().id))
    }

    @Transactional
    def updatePerfil(){
        def profesor = Profesor.findByUsuario(SecUser.get(springSecurityService.getPrincipal().id))
        profesor.properties=params.profesor

        if(profesor.hasErrors()){
            transactionStatus.setRollbackOnly()
            respond profesor.errors, view:'editPerfil'
            return
        }

        profesor.usuario.username= profesor.correoElectronico
        profesor.usuario.save flush:true
        profesor.save flush:true
        flash.message="Tus datos se han actualizado con exito"
       redirect action: "editPerfil"
    }

    def editDisponibilidad(){
        def profesor = Profesor.findByUsuario(SecUser.get(springSecurityService.getPrincipal().id))
        def convocatoria = Convocatoria.findByActual(true)
        def fechaActual= new Date()
        def preferencias= PreferenciaMaterias.findAllByProfesor(profesor)
        if(profesor.disponibilidad.size()==0){
            for (int i = 7; i < 21; i++) {
                profesor.addToDisponibilidad(new DisponibilidadProfesor(indice: 'L'+i, disponible: false))
                profesor.addToDisponibilidad(new DisponibilidadProfesor(indice: 'M'+i, disponible: false))
                profesor.addToDisponibilidad(new DisponibilidadProfesor(indice: 'X'+i, disponible: false))
                profesor.addToDisponibilidad(new DisponibilidadProfesor(indice: 'J'+i, disponible: false))
                profesor.addToDisponibilidad(new DisponibilidadProfesor(indice: 'V'+i, disponible: false))
            }
        }
        profesor.save flush: true
        def disponibilidad = DisponibilidadProfesor.findAllByProfesor(profesor,[sort:'indice'])
        def licenciaturas=Licenciatura.listOrderByNombre()
        if (convocatoria==null){
            redirect(action: 'index')
            return
        }
        if (fechaActual>profesor.fechaCierreProfesor){
            render view: 'periodoTerminado', model:[convocatoria: convocatoria, profesor: profesor]
            return
        }
        respond profesor: profesor, disponibilidad: disponibilidad, licenciaturas: licenciaturas, preferencias: preferencias, view: 'editDisponibilidad'
    }

    def updateDisponibilidad(){
        def profesor1 = Profesor.findByUsuario(SecUser.get(springSecurityService.getPrincipal().id))

        profesor1.properties= params.profesor
        PreferenciaMaterias.where{profesor==profesor1}.deleteAll();
        String[] preferencias = params.list('preferencias[]')
        for (String materia:preferencias){
            def nuevaPreferencia=new PreferenciaMaterias(profesor: profesor1, materia: Materia.get(Long.parseLong(materia))).save flush: true

        }
        profesor1.save flush: true

        flash.message ='Sus datos han sido guardados.'
        redirect action: 'editDisponibilidad'
    }

    def horario(){
        respond profesor: Profesor.findByUsuario(SecUser.get(springSecurityService.getPrincipal().id))
    }

    def ajaxListaMaterias(){
        def convocatoria=Convocatoria.findByActual(true)
        def semestres= new ArrayList<Integer>()
        if(convocatoria.periodo.equals("Agosto-Diciembre")){

            semestres.add(1)
            semestres.add(3)
            semestres.add(5)
            semestres.add(7)
            semestres.add(9)
        }else{
            semestres.add(2)
            semestres.add(4)
            semestres.add(6)
            semestres.add(8)
            semestres.add(10)
        }
        def materias= Materia.findAllByLicenciaturaAndSemestreInList(Licenciatura.findById(params.licenciatura),semestres,[sort: 'semestre'])
        render template: "listaMaterias", model: [materias: materias], contentType: 'text/plain'
    }

    def updateAvatar(){
        def profesor= Profesor.findByUsuario(SecUser.get(springSecurityService.getPrincipal().id))
        def webrootDir = servletContext.getRealPath("/");
        def imagen = request.getFile('avatar_file');
        def extenciones=["image/png","image/jpeg","image/jpg","image/gif"]
        if(!extenciones.contains(imagen.contentType)){
            flash.message='Formato de archivo no valido'
            redirect(action: 'miPerfil')
            return
        }
        if(!imagen.empty){
            String nombre="avatars/"
            switch (imagen.contentType){
                case "image/png":
                    nombre=nombre+profesor.id+".png";
                    break;
                case "image/jpeg":
                    nombre=nombre+profesor.id+".jpeg";
                    break;
                case "image/jpg":
                    nombre=nombre+profesor.id+".jpg";
                    break;
                case "image/gif":
                    nombre=nombre+profesor.id+".gif";
                    break;
            }
            File fileDest = new File(webrootDir,"images/"+nombre);
            imagen.transferTo(fileDest);
            profesor.avatar=nombre;

        }
        profesor.save(flush: true,failOnError: true);
        redirect(action: 'miPerfil')
    }

    def archivos(MateriaProfesorGrupo materiaProfesorGrupo){

        respond materiaProfesorGrupo: materiaProfesorGrupo, profesor: Profesor.findByUsuario(SecUser.get(springSecurityService.getPrincipal().id))
    }

    @Transactional
    def uploadArchivos(MateriaProfesorGrupo materiaProfesorGrupo){
        println(materiaProfesorGrupo.id)
        def webrootDir = servletContext.getRealPath("/");
        def planeacionMagisterial= request.getFile('planeacionMagisterial')
        def programaSintetico= request.getFile('programaSintetico')


        if(planeacionMagisterial.empty && programaSintetico.empty){
            flash.message='Favor de seleccionar los archivos correspondientes e intentarlo de nuevo'
            redirect(action: 'archivos', id: materiaProfesorGrupo.id)
            return
        }


        if(!planeacionMagisterial.empty && planeacionMagisterial.contentType.equals('application/vnd.openxmlformats-officedocument.wordprocessingml.document')){
            String nombre="profesor/planeacionMagisterial_"+materiaProfesorGrupo.materia.codigo+materiaProfesorGrupo.materia.nombre+'_'+materiaProfesorGrupo.profesor.apellidos+materiaProfesorGrupo.id+'.docx'
            File fileDest = new File(webrootDir,"documents/"+nombre)
            planeacionMagisterial.transferTo(fileDest)
            materiaProfesorGrupo.planeacionMagisterial=nombre
            materiaProfesorGrupo.save flush: true
        }

        if(! planeacionMagisterial.contentType.equals('application/vnd.openxmlformats-officedocument.wordprocessingml.document')){
            flash.message='El archivo de Planeacion Magisterial no es un archivo de Word (.docx)'
            redirect(action: 'archivos', id: materiaProfesorGrupo.id)
            return
        }

        if(!programaSintetico.empty && programaSintetico.contentType.equals('application/vnd.openxmlformats-officedocument.wordprocessingml.document')){
            String nombre="profesor/programaSintetico_"+materiaProfesorGrupo.materia.codigo+materiaProfesorGrupo.materia.nombre+'_'+materiaProfesorGrupo.profesor.apellidos+materiaProfesorGrupo.id+'.docx'
            File fileDest = new File(webrootDir,"documents/"+nombre)
            programaSintetico.transferTo(fileDest)
            materiaProfesorGrupo.programaSintetico=nombre
            materiaProfesorGrupo.save flush: true
        }
        if(! programaSintetico.contentType.equals('application/vnd.openxmlformats-officedocument.wordprocessingml.document')){
            flash.message='El archivo de Programa Sintetico no es un archivo de Word (.docx)'
            redirect(action: 'archivos', id: materiaProfesorGrupo.id)
            return
        }

        flash.message='Los archivos han sido guardados con exito'
        redirect(action: 'archivos', id: materiaProfesorGrupo.id)
    }


}
