package com.pahm

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ConvocatoriaController {
    static layout = "administrador"
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", updateArchivos: "POST"]

    def index(Integer max) {
       List<Convocatoria> convocatorias = Convocatoria.listOrderByAnio()
        respond convocatorias: convocatorias
    }

    def show(Convocatoria convocatoria) {
        respond convocatoria
    }

    def create() {
        respond new Convocatoria(params)
        def profesores=Profesor.listOrderByApellidos()
        respond profesores:profesores
    }

    @Transactional
    def save(Convocatoria convocatoria) {
        if (convocatoria == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (convocatoria.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond convocatoria.errors, view:'create'
            return
        }
        Convocatoria.executeUpdate("update Convocatoria set actual= FALSE")
        Profesor.executeUpdate("update Profesor set fecha_cierre_profesor= ?",[convocatoria.fechaCierre])
        List<Profesor> profesores= Profesor.findAllByActivo(true);
      for (Profesor profesor:profesores){
            sendMail {
                to profesor.correoElectronico
                from "no-reply@lockerz.mx"
                subject "Convocatoria "+convocatoria.periodo+" "+convocatoria.anio
                html view: "/emails/notificacionConvocatoria", model: [convocatoria: convocatoria]
            }
        }
        convocatoria.actual=true
        convocatoria.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'convocatoria.label', default: 'Convocatoria'), convocatoria.id])
                redirect convocatoria
            }
            '*' { respond convocatoria, [status: CREATED] }
        }
    }

    def edit(Convocatoria convocatoria) {
        respond convocatoria
    }

    @Transactional
    def update(Convocatoria convocatoria) {
        if (convocatoria == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (convocatoria.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond convocatoria.errors, view:'edit'
            return
        }

        convocatoria.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'convocatoria.label', default: 'Convocatoria'), convocatoria.id])
                redirect convocatoria
            }
            '*'{ respond convocatoria, [status: OK] }
        }
    }

    @Transactional
    def delete(Convocatoria convocatoria) {

        if (convocatoria == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        convocatoria.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'convocatoria.label', default: 'Convocatoria'), convocatoria.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'convocatoria.label', default: 'Convocatoria'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def archivos(){
        def convocatoria=Convocatoria.findByActual(true)
        if(convocatoria==null){
            flash.message='No existe ninguna convocatoria para realizar esta acción'
            redirect(action: 'index')
            return
        }
        respond convocatoria: convocatoria
    }
    @Transactional
    def uploadArchivos(){
        def convocatoria= Convocatoria.findByActual(true)
        def webrootDir = servletContext.getRealPath("/");
        def calendario= request.getFile('calendario')
        def herramientaPlaneacion= request.getFile('herramientaPlaneacion')
        def programaSintetico= request.getFile('programaSintetico')

        if(calendario.empty && herramientaPlaneacion.empty && programaSintetico.empty){
            flash.message='Favor de seleccionar los archivos correspondientes e intentarlo de nuevo'
            redirect(action: 'archivos')
            return
        }


        if(!calendario.empty && calendario.contentType.equals('application/pdf')){
            String nombre="convocatoria/CalendarioULSAOaxaca.pdf"
            File fileDest = new File(webrootDir,"documents/"+nombre)
            calendario.transferTo(fileDest)
            convocatoria.calendario=nombre
            convocatoria.save flush: true
        }

        if(! calendario.contentType.equals('application/pdf')){
            flash.message='El archivo de calendario no es un archivo PDF'
            redirect(action: 'archivos')
            return
        }

        if(!herramientaPlaneacion.empty && herramientaPlaneacion.contentType.equals('application/vnd.openxmlformats-officedocument.wordprocessingml.document')){
            String nombre="convocatoria/herramientaPlaneacionMagisterial.docx"
            File fileDest = new File(webrootDir,"documents/"+nombre)
            herramientaPlaneacion.transferTo(fileDest)
            convocatoria.herramientaPlaneacion=nombre
            convocatoria.save flush: true
        }
        if(! herramientaPlaneacion.contentType.equals('application/vnd.openxmlformats-officedocument.wordprocessingml.document')){
            flash.message='El archivo de Herramienta de Planeación Magisterial no es un archivo de Word (.docx)'
            redirect(action: 'archivos')
            return
        }

        if(!programaSintetico.empty && programaSintetico.contentType.equals('application/vnd.openxmlformats-officedocument.wordprocessingml.document')){
            String nombre="convocatoria/programaSintetico.docx"
            File fileDest = new File(webrootDir,"documents/"+nombre)
            programaSintetico.transferTo(fileDest)
            convocatoria.programaSintetico=nombre
            convocatoria.save flush: true
        }
        if(! programaSintetico.contentType.equals('application/vnd.openxmlformats-officedocument.wordprocessingml.document')){
            flash.message='El archivo de Programa Sintetico no es un archivo de Word (.docx)'
            redirect(action: 'archivos')
            return
        }

        flash.message='Los archivos han sido guardados con exito'
        redirect(action: 'archivos')
    }

}
