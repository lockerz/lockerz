package com.pahm

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AulaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    static layout = "administrador"
    def index(Integer max) {

        //respond Aula.list(params), model:[aulaCount: Aula.count()]
        List<Aula> aulas= Aula.listOrderByNombre();
        respond aulas:aulas
    }

    def show(Aula aula) {
        respond aula
    }

    def create() {
        respond new Aula(params)
    }

    @Transactional
    def save(Aula aula) {
        if (aula == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (aula.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond aula.errors, view:'create'
            return
        }
        aula.activo=true
        aula.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'aula.label', default: 'Aula'), aula.id])
                redirect aula
            }
            '*' { respond aula, [status: CREATED] }
        }
    }

    def edit(Aula aula) {
        respond aula
    }

    @Transactional
    def update(Aula aula) {
        if (aula == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (aula.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond aula.errors, view:'edit'
            return
        }

        aula.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'aula.label', default: 'Aula'), aula.id])
                redirect aula
            }
            '*'{ respond aula, [status: OK] }
        }
    }

    @Transactional
    def delete(Aula aula) {

        if (aula == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        List<Grupo> grupos=Grupo.findAllByAula(aula)
        for (Grupo grupo:grupos){
            grupo.aula=null
        }
        aula.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'aula.label', default: 'Aula'), aula.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'aula.label', default: 'Aula'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
