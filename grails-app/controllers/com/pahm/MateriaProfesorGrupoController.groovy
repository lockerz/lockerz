package com.pahm

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MateriaProfesorGrupoController {
    static layout = "administrador"
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        List<MateriaProfesorGrupo> materiaProfesorGrupos= MateriaProfesorGrupo.listOrderByGrupo(sort: 'grupo.nombre')
        respond materiaProfesorGrupos: materiaProfesorGrupos
    }

    def show(MateriaProfesorGrupo materiaProfesorGrupo) {
        respond materiaProfesorGrupo
    }

    def create() {
        def convocatoria=Convocatoria.findByActual(true)
        def semestres= new ArrayList<Integer>()
        if(convocatoria.periodo.equals("Agosto-Diciembre")){

            semestres.add(1)
            semestres.add(3)
            semestres.add(5)
            semestres.add(7)
            semestres.add(9)
        }else{
            semestres.add(2)
            semestres.add(4)
            semestres.add(6)
            semestres.add(8)
            semestres.add(10)
        }
        def materias= Materia.findAllBySemestreInList(semestres,[sort: 'nombre'])
        List<Profesor> profesores = Profesor.findAllByActivo(true)
        List<Grupo> grupos = Grupo.listOrderByLicenciatura()
        respond  materias:materias, profesores:profesores, grupos:grupos
    }

    @Transactional
    def save(MateriaProfesorGrupo materiaProfesorGrupo) {
        if (materiaProfesorGrupo == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (materiaProfesorGrupo.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond materiaProfesorGrupo.errors, view:'create'
            return
        }

        materiaProfesorGrupo.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'materiaProfesorGrupo.label', default: 'MateriaProfesorGrupo'), materiaProfesorGrupo.id])
                redirect materiaProfesorGrupo
            }
            '*' { respond materiaProfesorGrupo, [status: CREATED] }
        }
    }

    def edit(MateriaProfesorGrupo materiaProfesorGrupo) {
        def convocatoria=Convocatoria.findByActual(true)
        def semestres= new ArrayList<Integer>()
        if(convocatoria.periodo.equals("Agosto-Diciembre")){

            semestres.add(1)
            semestres.add(3)
            semestres.add(5)
            semestres.add(7)
            semestres.add(9)
        }else{
            semestres.add(2)
            semestres.add(4)
            semestres.add(6)
            semestres.add(8)
            semestres.add(10)
        }
        def materias= Materia.findAllBySemestreInList(semestres,[order: 'nombre'])
        List<Profesor> profesores = Profesor.listOrderByApellidos()
        List<Grupo> grupos = Grupo.listOrderByLicenciatura()
        respond materiaProfesorGrupo: materiaProfesorGrupo, materias:materias, profesores:profesores, grupos:grupos
    }

    @Transactional
    def update(MateriaProfesorGrupo materiaProfesorGrupo) {
        if (materiaProfesorGrupo == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (materiaProfesorGrupo.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond materiaProfesorGrupo.errors, view:'edit'
            return
        }

        materiaProfesorGrupo.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'materiaProfesorGrupo.label', default: 'MateriaProfesorGrupo'), materiaProfesorGrupo.id])
                redirect materiaProfesorGrupo
            }
            '*'{ respond materiaProfesorGrupo, [status: OK] }
        }
    }

    @Transactional
    def delete(MateriaProfesorGrupo materiaProfesorGrupo) {

        if (materiaProfesorGrupo == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        materiaProfesorGrupo.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'materiaProfesorGrupo.label', default: 'MateriaProfesorGrupo'), materiaProfesorGrupo.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'materiaProfesorGrupo.label', default: 'MateriaProfesorGrupo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def ajaxDisponibilidadProfesor(){
        def disponibilidad = DisponibilidadProfesor.findAllByProfesor(Profesor.get(params.profesor))
        def preferencias= PreferenciaMaterias.findAllByProfesor(Profesor.get(params.profesor))
        render template: "disponibilidad", model: [disponibilidad: disponibilidad, preferencias: preferencias], contentType: 'text/plain'

    }
}
