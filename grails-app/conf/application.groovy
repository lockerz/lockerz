// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.pahm.SecUser'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.pahm.SecUserSecRole'
grails.plugin.springsecurity.authority.className = 'com.pahm.SecRole'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/principal/**',               access: ['ROLE_Maestro','ROLE_Administrador']],
	[pattern: '/accounts/**',               access: ['permitAll']],
	[pattern: '/usuario/**',               access: ['ROLE_Maestro']],
	[pattern: '/profesor/**',        access: ['ROLE_Administrador']],
	[pattern: '/horariogrupo/**',        access: ['ROLE_Administrador']],
	[pattern: '/convocatoria/**',        access: ['ROLE_Administrador']],
	[pattern: '/administrador/**',        access: ['ROLE_Administrador']],
	[pattern: '/materiaprofesorgrupo/**',        access: ['ROLE_Administrador']],
	[pattern: '/materia/**',        access: ['ROLE_Administrador']],
	[pattern: '/aula/**',        access: ['ROLE_Administrador']],
	[pattern: '/grupo/**',        access: ['ROLE_Administrador']],
	[pattern: '/licenciatura/**', access: ['ROLE_Administrador']],
	[pattern: '/error',          access: ['permitAll']],
	[pattern: '/index',          access: ['permitAll']],
	[pattern: '/index.gsp',      access: ['permitAll']],
	[pattern: '/shutdown',       access: ['permitAll']],
	[pattern: '/assets/**',      access: ['permitAll']],
	[pattern: '/**/js/**',       access: ['permitAll']],
	[pattern: '/**/fonts/**',    access: ['permitAll']],
	[pattern: '/**/images/**',   access: ['permitAll']],
	[pattern: '/**/documents/**',   access: ['ROLE_Maestro','ROLE_Administrador']],
	[pattern: '/**/favicon.ico', access: ['IS_AUTHENTICATED_ANONYMOUSLY']]
]

grails.plugin.springsecurity.filterChain.chainMap = [
	[pattern: '/assets/**',      filters: 'none'],
	[pattern: '/**/js/**',       filters: 'none'],
	[pattern: '/**/fonts/**',    filters: 'none'],
	[pattern: '/**/images/**',   filters: 'none'],
	[pattern: '/**/favicon.ico', filters: 'none'],
	[pattern: '/**',             filters: 'JOINED_FILTERS']
]

grails.plugin.springsecurity.failureHandler.exceptionMappings = [
		[exception: 'org.springframework.security.authentication.CredentialsExpiredException', url: '/accounts/editPassword']
]

